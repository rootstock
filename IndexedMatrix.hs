module IndexedMatrix where
import Control.Monad (liftM)
import Data.Maybe (fromMaybe, isJust)
import Data.Set (Set)
import qualified Data.Set as Set
import Numeric.Matrix (Matrix, MatrixElement)
import qualified Numeric.Matrix as Matrix
import Util.Function ((...))
import qualified Util.Set as Set

data IndexedMatrix r c e = IndexedMatrix
  { rowIndices    :: Set r
  , columnIndices :: Set c
  , stripIndices  :: Matrix e
  } deriving (Show, Eq)

indexedMatrix :: MatrixElement e =>
  Set r -> Set c -> (r -> c -> e) -> IndexedMatrix r c e
indexedMatrix rs cs f = IndexedMatrix
  { rowIndices    = rs
  , columnIndices = cs
  , stripIndices  = Matrix.matrix (Set.size rs, Set.size cs) $ \(i, j) ->
      f (Set.elemAt (i - 1) rs) $ Set.elemAt (j - 1) cs
  }

maybeInRange :: (Ord r, Ord c) =>
  (IndexedMatrix r c e -> r -> c -> a) ->
    IndexedMatrix r c e -> r -> c -> Maybe a
maybeInRange f m r c =
  if (Set.member r (rowIndices m) && Set.member c (columnIndices m))
  then Just $ f m r c
  else Nothing

at :: (Ord r, Ord c, MatrixElement e) =>
  IndexedMatrix r c e -> r -> c -> Maybe e
at = maybeInRange $ \m r c -> Matrix.at (stripIndices m)
  (Set.findIndex r (rowIndices m) + 1, Set.findIndex c (columnIndices m) + 1)

at' :: (Ord r, Ord c, MatrixElement e) => IndexedMatrix r c e -> r -> c -> e
at' = fromMaybe 0 ... at

firstMinorMatrix :: (Ord r, Ord c, MatrixElement e) =>
  IndexedMatrix r c e -> r -> c -> Maybe (IndexedMatrix r c e)
firstMinorMatrix = maybeInRange $ \m r c ->
  let
    rs = rowIndices m
    cs = columnIndices m
  in
  IndexedMatrix
    { rowIndices    = Set.delete r rs
    , columnIndices = Set.delete c cs
    , stripIndices  = flip Matrix.minorMatrix (stripIndices m)
        (Set.findIndex r rs + 1, Set.findIndex c cs + 1)
    }

inverse :: MatrixElement e => IndexedMatrix r c e -> Maybe (IndexedMatrix c r e)
inverse m = flip liftM (Matrix.inv $ stripIndices m) $ \m' -> IndexedMatrix
  { rowIndices = columnIndices m
  , columnIndices = rowIndices m
  , stripIndices = m'
  }

invertible :: MatrixElement e => IndexedMatrix r c e -> Bool
invertible = isJust . inverse

identity :: MatrixElement e => Set i -> IndexedMatrix i i e
identity i = IndexedMatrix
  { rowIndices = i
  , columnIndices = i
  , stripIndices = Matrix.unit $ Set.size i
  }

times :: (Eq j, MatrixElement e) =>
  IndexedMatrix i j e -> IndexedMatrix j k e -> Maybe (IndexedMatrix i k e)
times m n =
  if columnIndices m == rowIndices n
  then Just $ IndexedMatrix
    { rowIndices = rowIndices m
    , columnIndices = columnIndices n
    , stripIndices = stripIndices m * stripIndices n
    }
  else Nothing

numRows :: IndexedMatrix r c e -> Integer
numRows = toInteger . Set.size . rowIndices

numColumns :: IndexedMatrix r c e -> Integer
numColumns = toInteger . Set.size . columnIndices

