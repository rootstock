--------------------------------------------------------------------------------
{-# LANGUAGE FlexibleContexts
  , GADTs
  , OverloadedStrings
  , QuasiQuotes
  , TemplateHaskell
  , TypeFamilies #-}

module Main
    ( main
    ) where


--------------------------------------------------------------------------------
import Prelude hiding (catch)
import Action
import Control.Applicative ((<$>), (<*>), pure)
import Control.Concurrent (forkIO)
import Control.Exception (AsyncException(..), catch, fromException)
import Control.Monad (forever, join, liftM, unless, when)
import Control.Monad.IfElse (aifM, awhenM, unlessM)
import Control.Monad.Trans (lift, liftIO)
import Control.Monad.Trans.Error (ErrorT(..), mapErrorT, throwError)
import Control.Monad.Trans.State
import Crypto.Random (SystemRandom, newGenIO)
import Crypto.Types.PubKey.ECDSA (PrivateKey)
import Data.Aeson
import Data.Aeson.Types
import Data.Base58Address (RippleAddress)
import qualified Data.Binary as B
import Data.Bits ((.|.))
import qualified Data.ByteString.Base16.Lazy as H
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BSL8
import Data.Foldable (forM_, toList)
import Data.List (find, intersperse)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (catMaybes, fromJust, fromMaybe, isJust, listToMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Time.Clock
import Data.Word (Word32)
import Database.Esqueleto hiding ((=.), get, update)
import Database.Persist.Postgresql hiding ((==.), (<=.), (!=.), get, update)
import qualified Database.Persist.Postgresql as P
import Database.Persist.TH
import Fund
import qualified Network.WebSockets as WS
import Numeric (showFFloat)
import qualified Ripple.Amount as RH
import Ripple.Seed (getSecret)
import Ripple.Sign (signTransaction)
import Ripple.Transaction
import Ripple.WebSockets (RippleResult(RippleResult))
import RootstockException (RootstockException(..))
import System.Environment (getArgs)
import Util.ApproxEq ((~~=))
import Util.Either (doLeft, isRight)
import Util.Error (throwIf)
import Util.Foldable (sumWith)
import Util.Function ((.!))
import Util.Monad ((>>=*), buildMap)
import Util.Persist (insertReturnEntity)
import Util.Set (distinctPairs, distinctPairsOneWay)
import ValueSimplex


--------------------------------------------------------------------------------
data AccountInfo = AccountInfo
  { dropsBalance    :: Integer
  , currentSequence :: Word32
  , transferRate    :: Double
  }

data IOUAmount = IOUAmount
  { iouLine       :: IOULine
  , iouQuantity   :: Double
  }
  deriving Show

newtype AccountLines = AccountLines [IOUAmount]

data Amount
  = Drops Integer
  | IOU IOUAmount
  deriving Show

data Offer = Offer
  { takerGets     :: Amount
  , takerPays     :: Amount
  , offerSequence :: Word32
  }

newtype Offers = Offers [Offer]

data BookOffer = BookOffer
  { bookOfferTakerGets        :: RH.Amount
  , bookOfferTakerPays        :: RH.Amount
  , bookOfferQuality          :: Double
  , bookOfferTakerGetsFunded  :: Maybe RH.Amount
  , bookOfferTakerPaysFunded  :: Maybe RH.Amount
  }

newtype BookOffers = BookOffers [BookOffer]

data Ledger = Ledger
  { ledgerIndex :: Integer
  , feeRef      :: Integer
  }

data RecordedTransaction = RecordedTransaction

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
  Node
    fund Fund
    NodeUnique fund
    deriving Eq
    deriving Ord
  FundStatus
    fundId    NodeId
    quantity  Double
    time      UTCTime
    FundStatusUnique fundId time
  HalfLink
    root      NodeId
    branch    NodeId
    quantity  Double
    time      UTCTime
    HalfLinkUnique root branch time
  ActionLog
    action      Action
    start       UTCTime
    end         UTCTime Maybe
    success     Bool Maybe
    ActionUnique start
  Warning
    warning Text
    time    UTCTime
  |]

type NodeEntity = Entity Node
type ValueSimplexND = ValueSimplex NodeEntity Double

data Rootstock = Rootstock
  { secret        :: PrivateKey
  , websocket     :: WS.Connection
  , sql           :: Connection
  , valueSimplex  :: ValueSimplexND
  , nextSequence  :: Word32
  , rsAction      :: ActionLogId
  , randGen       :: SystemRandom
  }

type RootstockIO = StateT Rootstock IO
type ExceptionalRootstock = ErrorT RootstockException RootstockIO


--------------------------------------------------------------------------------
instance ToJSON Amount where
  toJSON (Drops numDrops) = toJSON $ show numDrops
  toJSON (IOU iou) = object
    [ "currency"  .= lineCurrency (iouLine iou)
    , "issuer"    .= peerAccount  (iouLine iou)
    , "value"     .= showFFloat Nothing (iouQuantity iou) ""
    ]

instance FromJSON AccountInfo where
  parseJSON (Object obj) = do
    accountData <- obj .: "account_data"
    AccountInfo
      <$> (accountData .: "Balance"   >>= return . read)
      <*>  accountData .: "Sequence"
      <*> (maybe 1 (/1000000000) <$> accountData .:? "TransferRate")
  parseJSON value = fail $
    "Not an account info response:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON IOUAmount where
  parseJSON (Object obj) = IOUAmount
    <$> (IOULine
      <$> obj .: "account"
      <*> obj .: "currency")
    <*> (obj .: "balance" >>= return . read)
  parseJSON value = fail $
    "Not an account line:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON AccountLines where
  parseJSON (Object obj) = AccountLines <$> obj .: "lines"
  parseJSON value = fail $
    "Not a list of account lines:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON Amount where
  parseJSON (Object obj) = IOU <$> (IOUAmount
    <$> (IOULine
      <$> obj .: "issuer"
      <*> obj .: "currency")
    <*> (obj .: "value" >>= return . read))
  parseJSON (String str) = return $ Drops $ read $ T.unpack str
  parseJSON value = fail $
    "Not an Amount:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON Offer where
  parseJSON (Object obj) = Offer
    <$> obj   .: "taker_gets"
    <*> obj   .: "taker_pays"
    <*> obj   .: "seq"
  parseJSON value = fail $
    "Not an offer:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON Offers where
  parseJSON (Object obj) = Offers <$> obj .: "offers"
  parseJSON value = fail $
    "Not a list of offers:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON BookOffer where
  parseJSON (Object obj) = BookOffer
    <$> obj .:  "TakerGets"
    <*> obj .:  "TakerPays"
    <*> obj .:  "quality"
    <*> obj .:? "taker_gets_funded"
    <*> obj .:? "taker_pays_funded"
  parseJSON value = fail $ "Not a book offer:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON BookOffers where
  parseJSON (Object obj) = BookOffers <$> obj .: "offers"
  parseJSON value = fail $
    "Not a list of book offers:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON Ledger where
  parseJSON (Object obj) = Ledger
    <$> obj .: "ledger_index"
    <*> obj .: "fee_ref"
  parseJSON value = fail $
    "Not a ledger:\n" ++ (BSL8.unpack $ encode value)

instance FromJSON RecordedTransaction where
  parseJSON (Object obj) = do
    objType <- obj .: "type"
    if objType == ("transaction" :: Text)
    then return RecordedTransaction
    else fail $
      "Not a recorded transaction:\n" ++ (BSL8.unpack $ encode $ Object obj)
  parseJSON value = fail $
      "Not a recorded transaction:\n" ++ (BSL8.unpack $ encode value)


--------------------------------------------------------------------------------
secretFile, sqlPassFile :: FilePath
secretFile = "/media/mishael/ripple-secret"
sqlPassFile = "/media/mishael/sql-password"

connString :: BS.ByteString
connString = BS.concat
  [ "host=localhost port=5432 dbname=rootstock-test"
  , " user=rootstock password="
  ]
  
account :: Text
account  = "rpY4wdftAiEH5y5uzxC2XAvy3G27UyeQKS"

accountAddress :: RippleAddress
accountAddress = read $ T.unpack account

feeInDrops :: Integer
feeInDrops = 10000

fee :: RH.Amount
fee = RH.Amount (toRational feeInDrops / 1000000) RH.XRP

reserve :: Integer
reserve = 200000000

generosity, halfSpread :: Double
generosity = 1000000
halfSpread = 1.01

noAction :: ActionLogId
noAction = Key PersistNull

lookupXRP :: AccountInfo -> Amount
lookupXRP acInfo = Drops $ dropsBalance acInfo - reserve

lookupLine :: AccountLines -> IOULine -> Maybe Amount
lookupLine (AccountLines lines) fundLine = do
  foundLine <- find ((fundLine ==) . iouLine) lines
  return $ IOU foundLine

lookupFund :: AccountInfo -> AccountLines -> Fund -> Maybe Amount
lookupFund acInfo _ XRP = Just $ lookupXRP acInfo
lookupFund _ acLines (IOUFund fundLine) = lookupLine acLines fundLine

getQuantity :: Amount -> Double
getQuantity (Drops n) = fromInteger n
getQuantity (IOU iou) = iouQuantity iou

firstSequence :: [Field] -> Word32
firstSequence [] = 0
firstSequence (SequenceNumber x : _) = x
firstSequence (_:fs) = firstSequence fs

getSequence :: Transaction -> Word32
getSequence (Transaction fs) = firstSequence fs

nodeEntityFund :: NodeEntity -> Fund
nodeEntityFund = nodeFund . entityVal

lookupGetQuantity :: AccountInfo -> AccountLines -> NodeEntity -> Double
lookupGetQuantity acInfo acLines =
  fromMaybe 0 . liftM getQuantity .
    lookupFund acInfo acLines . nodeEntityFund

fromNodeEntity :: a -> (IOULine -> a) -> NodeEntity -> a
fromNodeEntity d f x = case nodeEntityFund x of
  XRP       -> d
  IOUFund l -> f l

amount :: Double -> NodeEntity -> Amount
amount q =
  fromNodeEntity (Drops $ round q) $ \l ->
    IOU $ IOUAmount {iouLine = l, iouQuantity = q}

peerOfNodeEntity :: NodeEntity -> Maybe Text
peerOfNodeEntity = fromNodeEntity Nothing $ Just . peerAccount

actionFinished :: ActionLog -> Bool
actionFinished = isJust . actionLogEnd

actionEntityFinished :: Entity ActionLog -> Bool
actionEntityFinished = actionFinished . entityVal

actionRunning :: Entity ActionLog -> Bool
actionRunning acEnt =
  actionLogAction (entityVal acEnt) == Running
  && not (actionEntityFinished acEnt)

updatedValueSimplexWithGenerosity ::
  Double -> ValueSimplexND -> AccountInfo -> AccountLines -> ValueSimplexND
updatedValueSimplexWithGenerosity gen vs acInfo acLines =
  multiUpdate vs $ \nodeEnt ->
    let actual = lookupGetQuantity acInfo acLines nodeEnt in
    case nodeEntityFund nodeEnt of
      XRP -> gen + actual
      _   -> actual

updatedValueSimplex ::
  ValueSimplexND -> AccountInfo -> AccountLines -> ValueSimplexND
updatedValueSimplex = updatedValueSimplexWithGenerosity 0

toRHAmount :: Amount -> RH.Amount
toRHAmount (Drops x) = RH.Amount (toRational x / 1000000) RH.XRP
toRHAmount (IOU x) = let
    line = iouLine x
    [a, b, c] = T.unpack $ lineCurrency line
  in
    RH.Amount (toRational $ iouQuantity x)
      $ RH.Currency (a, b, c) $ read $ T.unpack $ peerAccount line

commonTransactionStuff :: Word32 -> [Transaction] -> [Transaction]
commonTransactionStuff nextSeq = zipWith
  (\sequ (Transaction fs) -> Transaction $
    [ Account accountAddress
    , Fee fee
    , SequenceNumber sequ
    ] ++ fs
  )
  [nextSeq ..]

pivotFee ::
  ValueSimplexND -> Double -> NodeEntity -> Maybe (Double, ValueSimplexND)
pivotFee vs feeDrops x =
  let
    xrpNodeEntity =
      fromJust $ find ((XRP ==) . nodeEntityFund) $ toList $ nodes vs
  in
  if x == xrpNodeEntity
    then Just (feeDrops, vs)
    else if feeDrops >= supremumSellable vs xrpNodeEntity x
      then Nothing
      else let f = breakEven vs xrpNodeEntity (-feeDrops) x in
        Just (f, update vs xrpNodeEntity (-feeDrops) x f)

maybeOfferCreate
  :: ValueSimplexND
  -> ValueSimplexND
  -> NodeEntity
  -> NodeEntity
  -> (NodeEntity -> Double)
  -> Double
  -> Bool
  -> Maybe Transaction
maybeOfferCreate vs vs' x0 x1 trf f immediate = do
  (_, (q0, q1)) <-
    linkBreakEvenAtPriceWithFee vs' x0 x1 (halfSpread * price vs x0 x1) f
  return $ Transaction
    [ TransactionType OfferCreate
    , Flags $ tfSell .|. (if immediate then tfImmediateOrCancel else 0)
    , TakerPays $ toRHAmount $ amount q1 x1
    , TakerGets $ toRHAmount $ amount (-q0 / trf x0) x0
    ]

makeTransactions ::
  ValueSimplexND -> (NodeEntity -> Double) -> Word32 -> [Transaction]
makeTransactions vs trf nextSeq = commonTransactionStuff nextSeq $ do
  let xs = toList $ nodes vs
      n = toInteger $ length xs
  x1 <- xs
  flip (maybe [])
    (pivotFee vs (fromInteger $ (n * (n - 1) * 2 - 1) * feeInDrops) x1)
    $ \(f, vs') -> do
      x0 <- xs
      if x0 == x1
        then []
        else toList $ maybeOfferCreate vs vs' x0 x1 trf f False

--------------------------------------------------------------------------------
getSqlConnection :: RootstockIO Connection
getSqlConnection = gets sql

runSqlQuery :: SqlPersistM a -> RootstockIO a
runSqlQuery query = do
  sqlConn <- getSqlConnection
  lift $ runSqlPersistM query sqlConn

getNodeEntities :: SqlPersistM [NodeEntity]
getNodeEntities = select $ from return

readValueSimplexAt :: UTCTime -> SqlPersistM ValueSimplexND
readValueSimplexAt time = do
  nodeSet <- Set.fromList <$> getNodeEntities
  qMap <- buildMap (toList $ distinctPairs nodeSet) $ \(x, y) -> do
      [Value q] <- select $ from $ \hl -> do
        where_
          $   hl ^. HalfLinkRoot    ==. val (entityKey x)
          &&. hl ^. HalfLinkBranch  ==. val (entityKey y)
          &&. hl ^. HalfLinkTime    <=. val time
        orderBy [desc $ hl ^. HalfLinkTime]
        limit 1
        return $ hl ^. HalfLinkQuantity
      return q
  return $ fromFunction (curry $ flip (Map.findWithDefault 0) qMap) nodeSet

readValueSimplex :: SqlPersistM ValueSimplexND
readValueSimplex = liftIO getCurrentTime >>= readValueSimplexAt

writeValueSimplex ::
  AccountInfo -> AccountLines -> ValueSimplexND -> SqlPersistM ()
writeValueSimplex acInfo acLines vs = do
  time <- liftIO getCurrentTime
  insertMany $ flip map (toList $ nodes vs) $ \nodeEnt -> FundStatus
    { fundStatusFundId    = entityKey nodeEnt
    , fundStatusQuantity  = lookupGetQuantity acInfo acLines nodeEnt
    , fundStatusTime      = time
    }
  forM_ (distinctPairs $ nodes vs) $ \(x, y) -> insert_ $ HalfLink
      { halfLinkRoot      = entityKey x
      , halfLinkBranch    = entityKey y
      , halfLinkQuantity  = vsLookup vs x y
      , halfLinkTime      = time
      }

warn :: Text -> SqlPersistM ()
warn warning = do
  now <- liftIO getCurrentTime
  insert_ $ Warning
    { warningWarning  = warning
    , warningTime     = now
    }

getCurrentAction :: SqlPersistM (Maybe (Entity ActionLog))
getCurrentAction = liftM listToMaybe $ select $ from $ \ac -> do
  orderBy [desc $ ac ^. ActionLogStart]
  limit 1
  return ac

startAction :: Action -> SqlPersistM ActionLogId
startAction action = do
  start <- liftIO getCurrentTime
  insert $ ActionLog
    { actionLogAction   = action
    , actionLogStart    = start
    , actionLogEnd      = Nothing
    , actionLogSuccess  = Nothing
    }

endAction :: ActionLogId -> Bool -> SqlPersistM ()
endAction actionId success = do
  end <- liftIO getCurrentTime
  P.update actionId
    [ ActionLogEnd      =. Just end
    , ActionLogSuccess  =. Just success
    ]

putAction :: ActionLogId -> RootstockIO ()
putAction actionId = modify $ \rs -> rs {rsAction = actionId}

intervene :: Action -> ExceptionalRootstock () -> RootstockIO ()
intervene action intervention = do
  actionId <- runSqlQuery $ do
    awhenM getCurrentAction $ \curAc ->
      unless (actionEntityFinished curAc) $
        if actionLogAction (entityVal curAc) == Running
          then endAction (entityKey curAc) True
          else error "Another intervention appears to be running"
    startAction action
  putAction actionId
  result <- runErrorT intervention
  doLeft (lift . putStrLn . show) result
  runSqlQuery $ endAction actionId $ isRight result


--------------------------------------------------------------------------------
runWebsocket :: WS.ClientApp a -> RootstockIO a
runWebsocket app = gets websocket >>= lift . app

receiveData :: WS.WebSocketsData a => RootstockIO a
receiveData = runWebsocket WS.receiveData

sendTextData :: WS.WebSocketsData a => a -> RootstockIO ()
sendTextData x = runWebsocket $ flip WS.sendTextData x

waitForType :: FromJSON a => RootstockIO a
waitForType = do
  encoded <- receiveData
  case decode encoded of
    Nothing -> do
      lift $ putStrLn ("Skipping:\n" ++ (BSL8.unpack encoded))
      waitForType
    Just result -> do
      lift $ putStrLn ("Using:\n" ++ (BSL8.unpack encoded))
      return result

waitForResponseWithId :: (Eq id, FromJSON id, FromJSON a)
  => id -> RootstockIO (Maybe a)
waitForResponseWithId idSought = do
  RippleResult i x <- waitForType
  if i == Just idSought
    then return $ either (const Nothing) Just x
    else waitForResponseWithId idSought

askUntilAnswered :: FromJSON a => [Pair] -> RootstockIO a
askUntilAnswered question = do
  qTime <- show <$> liftIO getCurrentTime
  sendTextData $ encode $ object $ ("id" .= qTime) : question
  aifM (waitForResponseWithId qTime) return $ do
    waitForType :: RootstockIO Ledger
    askUntilAnswered question

signAndSubmit :: Transaction -> RootstockIO ()
signAndSubmit tx = do
  Right (txSigned, rGen) <- signTransaction tx <$> gets secret <*> gets randGen
  modify $ \rs -> rs {randGen = rGen}
  sendTextData $ encode $ object
    [ "command" .= ("submit" :: Text)
    , "tx_blob" .= BSL8.unpack (H.encode $ B.encode txSigned)
    ]

subscribe :: [Pair] -> WS.ClientApp ()
subscribe options = 
  flip WS.sendTextData $ encode $ object $
    ["command"   .= ("subscribe" :: Text)] ++ options

subscribeLedger :: WS.ClientApp ()
subscribeLedger = subscribe ["streams" .= ["ledger" :: Text]]

subscribeAccount :: WS.ClientApp ()
subscribeAccount = subscribe ["accounts" .= [account]]

subscribeLedgerAndAccount :: WS.ClientApp()
subscribeLedgerAndAccount = subscribe
    [ "streams"   .= ["ledger" :: Text]
    , "accounts"  .= [account]
    ]

queryOwnAccount :: FromJSON a => Text -> RootstockIO a
queryOwnAccount command = askUntilAnswered
    [ "command" .= command
    , "account" .= account
    , "ledger_index" .= ("validated" :: Text)
    ]

getAccountInfo :: RootstockIO AccountInfo
getAccountInfo = queryOwnAccount "account_info"

getAccountLines :: RootstockIO AccountLines
getAccountLines = queryOwnAccount "account_lines"

getAccountOffers :: RootstockIO Offers
getAccountOffers = queryOwnAccount "account_offers"

getCurrentAccountInfo :: Text -> RootstockIO AccountInfo
getCurrentAccountInfo peer = askUntilAnswered
    [ "command" .= ("account_info" :: Text)
    , "account" .= peer
    , "ledger_index" .= ("current" :: Text)
    ]

valueSimplexEmpty :: RootstockIO Bool
valueSimplexEmpty = isEmpty <$> gets valueSimplex

putValueSimplex :: ValueSimplexND -> RootstockIO ()
putValueSimplex vs = modify $ \rs -> rs {valueSimplex = vs}

putSequence :: Word32 -> RootstockIO ()
putSequence nextSeq = modify $ \rs -> rs {nextSequence = nextSeq}

getAndPutSequence :: RootstockIO ()
getAndPutSequence =
  currentSequence <$> getCurrentAccountInfo account >>= putSequence

ownActionGoingQuery :: RootstockIO (SqlPersistM Bool)
ownActionGoingQuery = do
  actId <- gets rsAction
  return $ maybe False (not . actionFinished) <$> P.get actId

ifRunning :: SqlPersistM () -> ExceptionalRootstock ()
ifRunning query = do
  goingQ <- lift ownActionGoingQuery
  mapErrorT runSqlQuery $ do
    going <- lift $ goingQ
    throwIf NotRunning $ not going
    lift query

checkRunning :: ExceptionalRootstock ()
checkRunning = ifRunning $ return ()

submitUntilSequenceCatchup' :: [Transaction] -> ExceptionalRootstock ()
submitUntilSequenceCatchup' txs = unless (null txs) $ do
  checkRunning
  forM_ txs $ lift . signAndSubmit
  lift (waitForType :: RootstockIO Ledger)
  curSeq <- currentSequence <$> lift getAccountInfo
  submitUntilSequenceCatchup' $ dropWhile ((curSeq >) . getSequence) txs

submitUntilSequenceCatchup :: [Transaction] -> ExceptionalRootstock ()
submitUntilSequenceCatchup txs = do
  lift $ putSequence =<< (fromIntegral (length txs) +) <$> gets nextSequence
  submitUntilSequenceCatchup' txs

clearAndUpdate :: ExceptionalRootstock ()
{- Must have subscribed to ledger updates for this to work -}
clearAndUpdate = do
  Offers offerList <- lift getAccountOffers
  if null offerList
  then do
    acInfo <- lift getAccountInfo
    acLines <- lift getAccountLines
    vs <- lift $ gets valueSimplex
    let vs' = updatedValueSimplex vs acInfo acLines
    when (status (~~=) vs' /= OK) $ error "Invalid updated ValueSimplex!"
    ifRunning $ do
      unless (strictlySuperior (~~=) vs' vs) $ do
        let
          vs'' = updatedValueSimplexWithGenerosity generosity vs acInfo acLines
          warning
            =           " non-superior ValueSimplex (generosity: "
            `T.append`  T.pack (show generosity)
            `T.append`  ")"
        if strictlySuperior (~~=) vs'' vs
          then warn $ "Slightly" `T.append` warning
          else error $ "Seriously" ++ T.unpack warning
      writeValueSimplex acInfo acLines vs'
    lift $ putValueSimplex vs'
  else do
    curSeq <- lift $ gets nextSequence
    submitUntilSequenceCatchup $ commonTransactionStuff curSeq $
      flip map offerList $ \off -> Transaction
        [ TransactionType OfferCancel
        , OfferSequence $ offerSequence off
        ]
    clearAndUpdate

strictlySuperiorToCurrent :: ValueSimplexND -> RootstockIO Bool
strictlySuperiorToCurrent vs' = strictlySuperior (~~=) vs' <$> gets valueSimplex

waitForImprovement :: ExceptionalRootstock ()
waitForImprovement = do
  checkRunning
  Offers offerList <- lift getAccountOffers
  unlessM
    (lift $ strictlySuperiorToCurrent =<<
      (updatedValueSimplexWithGenerosity
        (fromInteger $ (negate feeInDrops *) $ toInteger $ length offerList)
        <$> gets valueSimplex
        <*> getAccountInfo
        <*> getAccountLines
    )) $ do
    lift (waitForType :: RootstockIO Ledger)
    lift (waitForType :: RootstockIO RecordedTransaction)
    waitForImprovement

submitAndWait :: [Transaction] -> ExceptionalRootstock ()
submitAndWait txs = do
  submitUntilSequenceCatchup txs
  waitForImprovement

getTransitRates :: RootstockIO (NodeEntity -> Double)
getTransitRates = do
  peers <- catMaybes . toList . Set.map peerOfNodeEntity . nodes
    <$> gets valueSimplex
  trm <- buildMap peers $ \peer -> transferRate <$> getCurrentAccountInfo peer
  return $ \x -> fromMaybe 1 $ peerOfNodeEntity x >>= flip Map.lookup trm

startRunning :: RootstockIO ()
startRunning = do
  mavs <- runSqlQuery $ do
    mcurAc <- getCurrentAction
    case mcurAc of
      Nothing     -> error $ show DatabaseNotSetUp
      Just curAc  ->
        if actionEntityFinished curAc
          then do
            actId <- startAction Running
            vs <- readValueSimplex
            return $ Just (actId, vs)
          else return Nothing
  case mavs of
    Nothing -> do
      waitForType :: RootstockIO Ledger
      startRunning
    Just (actId, vs) -> do
      putAction actId
      putValueSimplex vs
      getAndPutSequence

ensureRunning :: RootstockIO ()
ensureRunning =
  unlessM (join $ runSqlQuery <$> ownActionGoingQuery)
    startRunning

marketMakerLoop :: RootstockIO ()
marketMakerLoop = do
  runErrorT $ do
    clearAndUpdate
    lift
        ( makeTransactions
        <$> gets valueSimplex
        <*> getTransitRates
        <*> gets nextSequence
        )
      >>= submitAndWait
  ensureRunning
  marketMakerLoop


--------------------------------------------------------------------------------
getLineBal :: AccountLines -> IOULine -> ExceptionalRootstock Double
getLineBal acLines fundLine = do
  lineBal <- case lookupLine acLines fundLine of
    Nothing -> throwError LineNotFound
    Just amount -> return $ getQuantity amount
  throwIf NonPositiveLine $ lineBal <= 0
  return lineBal

setupDatabase :: IOULine -> ExceptionalRootstock ()
setupDatabase fundLine = do
  isEmpt <- lift $ valueSimplexEmpty
  throwIf DatabaseExists $ not isEmpt
  lift $ runWebsocket subscribeLedger
  acInfo <- lift getAccountInfo
  let dropsBal = getQuantity $ lookupXRP acInfo
  throwIf InsufficientForReserve $ dropsBal <= 0
  acLines <- lift getAccountLines
  lineBal <- getLineBal acLines fundLine
  lift $ runSqlQuery $ do
    xrpNodeEntity   <- insertReturnEntity $ Node {nodeFund = XRP}
    lineNodeEntity  <- insertReturnEntity $ Node {nodeFund = IOUFund fundLine}
    writeValueSimplex acInfo acLines $
      flip fromFunction (Set.fromList [xrpNodeEntity, lineNodeEntity]) $ \x _ ->
        if x == xrpNodeEntity
        then dropsBal
        else lineBal

addCurrency :: IOULine -> Double -> ExceptionalRootstock ()
addCurrency fundLine priceInDrops = do
  mxrpNodeEntity <- lift $ runSqlQuery $ getBy $ NodeUnique XRP
  xrpNodeEntity <- maybe (throwError DatabaseNotSetUp) return mxrpNodeEntity
  throwIf NonPositivePrice $ priceInDrops <= 0
  let lineFund = IOUFund fundLine
  alreadyPresent <-
    isJust <$> (lift $ runSqlQuery $ getBy $ NodeUnique lineFund)
  throwIf CurrencyAlreadyPresent alreadyPresent
  lift $ runWebsocket subscribeLedgerAndAccount
  lift $ getAndPutSequence
  clearAndUpdate
  acLines <- lift getAccountLines
  lineBal <- getLineBal acLines fundLine
  vs <- lift $ gets valueSimplex
  throwIf NewOutweighsOld $
    priceInDrops * lineBal >= totalValue vs xrpNodeEntity
  acInfo <- lift getAccountInfo
  lift $ runSqlQuery $ do
    lineNodeEntity <- insertReturnEntity $ Node {nodeFund = lineFund}
    writeValueSimplex acInfo acLines $
      addNode vs lineNodeEntity lineBal xrpNodeEntity priceInDrops

report :: RootstockIO ()
report = do
  now <- liftIO getCurrentTime
  (vs, lastInterventionTime) <- runSqlQuery $ do
    [Value (Just lastInterventionTime)] <- select $ from $ \acEnt -> do
      where_ $ acEnt ^. ActionLogAction !=. val Running
      orderBy [desc $ acEnt ^. ActionLogStart]
      limit 1
      return $ acEnt ^. ActionLogEnd
    vs <- readValueSimplexAt lastInterventionTime
    return (vs, lastInterventionTime)
  vs' <- gets valueSimplex
  let xs = nodes vs
  liftIO $ do
    let xys = distinctPairsOneWay xs
        v = halfLinkValue vs
        v' = halfLinkValue vs'
        recipYears =
          60 * 60 * 24 * 365
            / (fromRational $ toRational $
              diffUTCTime now lastInterventionTime)
        x0 = Set.findMin xs
        x0Value = totalValue vs' x0
        x0Gain = flip sumWith xys $ \(x, y) ->
          2 * hybridPrice vs' x y x0 * (v' x y - v x y)
    forM_ xys $ \(x, y) -> mapM_ putStrLn
      [ show $ nodeEntityFund x
      , show $ nodeEntityFund y
      , show $
        (v' x y / v x y)
          ** recipYears
      , show $ 2 * hybridPrice vs' x y x0 * v' x y / x0Value
      , ""
      ]
    forM_ xs $ \x -> mapM_ putStrLn
      [ show $ nodeEntityFund x
      , show $ totalValue vs' x
      , show $ x0Gain / price vs' x x0
      , show $ nodeValue vs' x / totalValue vs' x
      , ""
      ]
    putStrLn $ show $ (1 + x0Gain / x0Value) ** recipYears

promptDeposit :: ExceptionalRootstock ()
promptDeposit = do
  liftIO $ putStrLn "Please wait while I cancel all offers ..."
  lift $ runWebsocket subscribeLedger
  lift getAndPutSequence
  clearAndUpdate
  liftIO $ mapM_ putStrLn
    [ "... Finished!"
    , "Please deposit into account "
      ++ T.unpack account
      ++ " and press Enter to continue."
    ]
  liftIO getLine
  lift (waitForType :: RootstockIO Ledger)
  vs <- lift $ gets valueSimplex
  acInfo <- lift getAccountInfo
  acLines <- lift getAccountLines
  lift $ runSqlQuery $ writeValueSimplex acInfo acLines $
    deposit vs $ lookupGetQuantity acInfo acLines


--------------------------------------------------------------------------------
runRootstock :: RootstockIO a -> Rootstock -> IO a
runRootstock = evalStateT

marketMaker :: RootstockIO ()
marketMaker = do
  isEmpt <- valueSimplexEmpty
  when isEmpt $ error $ show DatabaseNotSetUp
  runWebsocket subscribeLedgerAndAccount
  startRunning
  rs <- get
  liftIO $ catch (runRootstock marketMakerLoop rs) $ \e -> do
      flip runSqlPersistM (sql rs) $ do
        curAc <- fromJust <$> getCurrentAction
        if actionRunning curAc
          then
            endAction
              (entityKey curAc)
              $ fromException e `elem` map Just [ThreadKilled, UserInterrupt]
          else return ()
      putStrLn $ "Exiting on: " ++ show e

rippleInteract :: WS.ClientApp ()
rippleInteract conn = do
  -- Fork a thread that writes WS data to stdout
  _ <- forkIO $ forever $ do
      msg <- WS.receiveData conn
      liftIO $ T.putStrLn msg

  runRipple subscribeAccount

  -- Read from stdin and write to WS
  let loop = do
          line <- T.getLine
          unless (T.null line) $ WS.sendTextData conn line >> loop

  loop
  WS.sendClose conn ("Bye!" :: Text)

readSecret :: IO String
readSecret = readFile secretFile

readSqlPass :: IO BS.ByteString
readSqlPass = BS.pack <$> readFile sqlPassFile

runRipple :: WS.ClientApp a -> IO a
runRipple app = WS.runClient "127.0.0.1" 5006 "/" app

runRippleWithSecret :: RootstockIO a -> IO a
runRippleWithSecret app = do
  sec <- readSecret
  sqlPass <- readSqlPass
  rGen <- newGenIO
  withPostgresqlConn (BS.concat [connString, sqlPass]) $ \sqlConn -> do
    vs <- flip runSqlPersistM sqlConn $ do
      runMigration migrateAll
      readValueSimplex
    runRipple $ \wsConn ->
      runRootstock app $ Rootstock
        { websocket     = wsConn
        , secret        = getSecret $ read sec
        , sql           = sqlConn
        , valueSimplex  = vs
        , nextSequence  = 0
        , rsAction      = noAction
        , randGen       = rGen
        }

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["setup", currency, peer] -> runRippleWithSecret $ intervene InitialSetup $
      setupDatabase $ IOULine
        { peerAccount   = T.pack peer
        , lineCurrency  = T.pack currency
        }
    ["run"] -> runRippleWithSecret marketMaker
    ["addCurrency", currency, peer, priceInXRP] ->
      runRippleWithSecret $ intervene AddNode $ addCurrency
        ( IOULine
          { peerAccount   = T.pack peer
          , lineCurrency  = T.pack currency
          }
        )
        $ read priceInXRP * 1000000
    ["report"] -> runRippleWithSecret report
    ["interact"] -> runRipple rippleInteract
    ["deposit"] -> runRippleWithSecret $ intervene Deposit $ promptDeposit
    _ -> putStrLn "Command not understood"

