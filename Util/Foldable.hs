module Util.Foldable where
import Data.Foldable (Foldable, foldMap)
import Data.Monoid (Sum(Sum, getSum))

sumWith :: (Foldable t, Num b) => (a -> b) -> t a -> b
sumWith f = getSum . foldMap (Sum . f)

