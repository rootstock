module Util.Monad where
import Control.Monad (foldM)
import Data.Map (Map)
import qualified Data.Map as Map

infixl 1 >>=*
(>>=*) :: Monad m => a -> [a -> m a] -> m a
(>>=*) = foldM $ flip id

buildMap :: (Ord a, Monad m) => [a] -> (a -> m b) -> m (Map a b)
buildMap xs f = Map.empty >>=* (flip map xs $ \x m -> do
  y <- f x
  return $ Map.insert x y m)

