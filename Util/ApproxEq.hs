module Util.ApproxEq where
import Data.Eq.Approximate
import Data.Function (on)
import TypeLevel.NaturalNumber

type RelApproxDouble =
  RelativelyApproximateValue (Digits Twelve) (Digits Eight) Double
wrapRelAD :: Double -> RelApproxDouble
wrapRelAD = RelativelyApproximateValue
unwrapRelAD :: RelApproxDouble -> Double
unwrapRelAD = unwrapRelativelyApproximateValue

infix 4 ~~=
class RelApproxEq a where
  (~~=) :: a -> a -> Bool

instance RelApproxEq Double where
  (~~=) = (==) `on` wrapRelAD

instance RelApproxEq a => RelApproxEq (Maybe a) where
  Nothing ~~= Nothing  = True
  Just x  ~~= Just y   = x ~~= y
  _       ~~= _        = False

