module Util.Monotonic
  ( monotonicWordToDouble
  , monotonicDoubleToWord
  ) where

import Data.Bits
import Data.ReinterpretCast (doubleToWord, wordToDouble)
import Data.Word (Word64)

checkInfiniteOrNaN :: Word64 -> Word64
checkInfiniteOrNaN w = if shiftR w 52 == 0x7ff
  then 0x7fefffffffffffff
  else w

monotonicWordToDouble :: Word64 -> Double
monotonicWordToDouble w = wordToDouble $ if testBit w 63
  then checkInfiniteOrNaN $ clearBit w 63
  else flip setBit 63 $ checkInfiniteOrNaN $ flip clearBit 63 $ complement w

monotonicDoubleToWord :: Double -> Word64
monotonicDoubleToWord x = let w = doubleToWord x in
  if testBit w 63
    then complement w
    else setBit w 63

