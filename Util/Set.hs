module Util.Set where
import qualified Data.List as List
import Data.Maybe (fromJust)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Tuple (swap)

subsetsOfSize :: Ord a => Integer -> Set a -> Set (Set a)
subsetsOfSize n xs = case compare n 0 of
  LT -> Set.empty
  EQ -> Set.singleton Set.empty
  GT -> case Set.minView xs of
    Nothing -> Set.empty
    Just (x, xs') -> Set.union
      (subsetsOfSize n xs') $
      Set.map (Set.insert x) $ subsetsOfSize (n - 1) xs'

distinctPairsOneWay :: Ord a => Set a -> Set (a, a)
distinctPairsOneWay =
  Set.map (\xys -> let [x, y] = Set.toList xys in (x, y)) . subsetsOfSize 2

distinctPairs :: Ord a => Set a -> Set (a, a)
distinctPairs xs =
  let dpow = distinctPairsOneWay xs in
  Set.union dpow $ Set.map swap dpow

distinctTriplesOneWay :: Ord a => Set a -> Set (a, a, a)
distinctTriplesOneWay =
  Set.map (\xyzs -> let [x, y, z] = Set.toList xyzs in (x, y, z))
  . subsetsOfSize 3

