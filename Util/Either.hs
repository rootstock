module Util.Either where

isLeft :: Either a b -> Bool
isLeft (Left _) = True
isLeft _        = False

isRight :: Either a b -> Bool
isRight = not . isLeft

doLeft :: Monad m => (a -> m ()) -> Either a b -> m ()
doLeft f (Left x) = f x
doLeft _ _        = return ()

