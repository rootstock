{-# LANGUAGE TypeFamilies #-}
module Util.Persist where
import Database.Persist.Class
import Database.Persist.Types

insertReturnEntity ::
  ( PersistStore m
  , PersistMonadBackend m ~ PersistEntityBackend val
  , PersistEntity val)
  => val -> m (Entity val)
insertReturnEntity x = do
  key <- insert x
  return $ Entity {entityKey = key, entityVal = x}

