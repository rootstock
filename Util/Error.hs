module Util.Error where
import Control.Monad.Trans.Error (Error, ErrorT, throwError)

throwIf :: (Error e, Monad m) => e -> Bool -> ErrorT e m ()
throwIf err test = if test then throwError err else return ()

