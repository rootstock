module RootstockException where
import Control.Monad.Trans.Error (Error, strMsg)

data RootstockException
  = NotRunning
  | DatabaseExists
  | InsufficientForReserve
  | LineNotFound
  | NonPositiveLine
  | DatabaseNotSetUp
  | NonPositivePrice
  | CurrencyAlreadyPresent
  | NewOutweighsOld
  | StrMsg String

instance Show RootstockException where
  show NotRunning
    = "According to the database, Rootstock shouldn't be running now"
  show DatabaseExists
    = "Database already has nodes"
  show InsufficientForReserve
    = "Not enough XRP to meet desired reserve"
  show LineNotFound
    = "Requested IOU line not found"
  show NonPositiveLine
    = "Non-positive balance in requested IOU line"
  show DatabaseNotSetUp
    = "The database doesn't seem to have been set up yet"
  show NonPositivePrice
    = "The price given should be positive, but isn't"
  show CurrencyAlreadyPresent
    = "The specified currency is already being managed by Rootstock"
  show NewOutweighsOld
    = "The currency to be added is too much to fit into the current Rootstock"
  show (StrMsg s) = s

instance Error RootstockException where
  strMsg = StrMsg

