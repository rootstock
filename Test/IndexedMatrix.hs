{-# LANGUAGE TemplateHaskell #-}
module Test.IndexedMatrix where
import Prelude hiding (all)
import Control.Applicative ((<$>), (<*>))
import Data.Eq.Approximate
import Data.Foldable (all)
import Data.Function (on)
import Data.Maybe (fromJust, isJust, isNothing)
import Data.Set (Set)
import qualified Data.Set as Set
import IndexedMatrix
import Numeric.Matrix (MatrixElement)
import qualified Numeric.Matrix as Matrix
import Test.QuickCheck
import Test.QuickCheck.All
import Test.Set
import TypeLevel.NaturalNumber
import Util.Function ((.!))

type ApproximateDouble = AbsolutelyApproximateValue (Digits Eight) Double
wrapAD :: Double -> ApproximateDouble
wrapAD = AbsolutelyApproximateValue
unwrapAD :: ApproximateDouble -> Double
unwrapAD = unwrapAbsolutelyApproximateValue

infix 4 ~=
class ApproxEq a where
  (~=) :: a -> a -> Bool

instance ApproxEq Double where
  (~=) = (==) `on` wrapAD

instance ApproxEq a => ApproxEq (Maybe a) where
  Nothing ~= Nothing  = True
  Just x  ~= Just y   = x ~= y
  _       ~= _        = False

instance (Ord r, Ord c, MatrixElement e, ApproxEq e) =>
  ApproxEq (IndexedMatrix r c e) where
  a ~= b =
    let
      rs = rowIndices a
      cs = columnIndices a
    in rowIndices b == rs
    && columnIndices b == cs
    && (flip all rs $ \r -> flip all cs $ \c -> at a r c ~= at b r c)

instance
  ( Arbitrary r, CoArbitrary r, Ord r
  , Arbitrary c, CoArbitrary c, Ord c
  , Arbitrary e, MatrixElement e
  ) => Arbitrary (IndexedMatrix r c e) where
  arbitrary = indexedMatrix
    <$> arbitrarySetOfSizeSqrt
    <*> arbitrarySetOfSizeSqrt
    <*> arbitrary

arbitrarySquareMatrix ::
  ( Arbitrary r, CoArbitrary r, Ord r
  , Arbitrary c, CoArbitrary c, Ord c
  , Arbitrary e, MatrixElement e
  ) => Gen (IndexedMatrix r c e)
arbitrarySquareMatrix = do
  rs <- arbitrarySetOfSizeSqrt
  cs <- arbitrarySetOfExactSize $ Set.size rs
  indexedMatrix rs cs <$> arbitrary

prop_valid_indexedMatrix :: (Ord r, Ord c, MatrixElement e) =>
  IndexedMatrix r c e -> Bool
prop_valid_indexedMatrix m = let m' = stripIndices m
  in Set.size (rowIndices m) == Matrix.numRows m'
  && Set.size (columnIndices m) == Matrix.numCols m'
  && m == indexedMatrix (rowIndices m) (columnIndices m) (at' m)

withIndicesIn :: (Ord r, Show r, Ord c, Show c, Testable p) =>
  IndexedMatrix r c e -> (r -> c -> p) -> Property
withIndicesIn m f = withArbitraryElement (rowIndices m) $
  withArbitraryElement (columnIndices m) . f
  
prop_at_in :: (Ord r, Show r, Ord c, Show c, MatrixElement e) =>
  IndexedMatrix r c e -> Property
prop_at_in m = withIndicesIn m $ isJust .! at m

prop_at_out :: (Ord r, Ord c, MatrixElement e) =>
  IndexedMatrix r c e -> r -> c -> Property
prop_at_out m r c =
  not (Set.member r (rowIndices m) && Set.member c (columnIndices m)) ==>
    isNothing $ at m r c

prop_firstMinorMatrix_valid_indexedMatrix ::
  (Ord r, Show r, Ord c, Show c, MatrixElement e) =>
  IndexedMatrix r c e -> Property
prop_firstMinorMatrix_valid_indexedMatrix m = withIndicesIn m $
  (prop_valid_indexedMatrix . fromJust) .! firstMinorMatrix m

prop_firstMinorMatrix_size :: (Ord r, Show r, Ord c, Show c, MatrixElement e) =>
  IndexedMatrix r c e -> Property
prop_firstMinorMatrix_size m = withIndicesIn m $ \r c ->
  let m' = fromJust $ firstMinorMatrix m r c in
  rowIndices m' == Set.delete r (rowIndices m) &&
    columnIndices m' == Set.delete c (columnIndices m)

prop_firstMinorMatrix_entries ::
  (Ord r, Show r, Ord c, Show c, MatrixElement e) =>
  IndexedMatrix r c e -> Property
prop_firstMinorMatrix_entries m = withIndicesIn m $ \r c ->
  let m' = fromJust $ firstMinorMatrix m r c in
  withIndicesIn m' $ \r' c'
    -> at m' r' c' == at m r' c'
    && (isJust $ at m' r' c')
    && (isNothing $ at m' r c')
    && (isNothing $ at m' r' c)
    && (isNothing $ at m' r c)

squareMatrixTest ::
  ( Arbitrary r, CoArbitrary r, Ord r, Show r
  , Arbitrary c, CoArbitrary c, Ord c, Show c
  , Arbitrary e, MatrixElement e, Show e
  , Testable p
  ) => (IndexedMatrix r c e -> p) -> Property
squareMatrixTest = forAll arbitrarySquareMatrix

squareMatrixTestDouble :: Testable p =>
  (IndexedMatrix Integer Integer Double -> p) -> Property
squareMatrixTestDouble = squareMatrixTest

squareMatrixTestRational :: Testable p =>
  (IndexedMatrix Integer Integer Rational -> p) -> Property
squareMatrixTestRational = squareMatrixTest

prop_invertible_Double :: Property
prop_invertible_Double = squareMatrixTestDouble invertible

prop_invertible_Rational :: Property
prop_invertible_Rational = squareMatrixTestRational invertible

inverse_valid_indexedMatrix :: (Ord r, Ord c, MatrixElement e) =>
  IndexedMatrix r c e -> Property
inverse_valid_indexedMatrix m =
  invertible m ==> prop_valid_indexedMatrix $ fromJust $ inverse m

prop_inverse_valid_indexedMatrix_Double :: Property
prop_inverse_valid_indexedMatrix_Double =
  squareMatrixTestDouble inverse_valid_indexedMatrix

prop_inverse_valid_indexedMatrix_Rational :: Property
prop_inverse_valid_indexedMatrix_Rational =
  squareMatrixTestRational inverse_valid_indexedMatrix

inverse_times_eq_id :: (Eq r, Eq c, MatrixElement e) =>
  IndexedMatrix r c e -> Property
inverse_times_eq_id m = invertible m
  ==> m `times` m' == Just (identity $ rowIndices m)
  &&  m' `times` m == Just (identity $ columnIndices m)
  where m' = fromJust $ inverse m

inverse_times_approxeq_id :: (Ord r, Ord c, MatrixElement e, ApproxEq e) =>
  IndexedMatrix r c e -> Property
inverse_times_approxeq_id m = invertible m
  ==> m `times` m' ~= Just (identity $ rowIndices m)
  &&  m' `times` m ~= Just (identity $ columnIndices m)
  where m' = fromJust $ inverse m

prop_inverse_times_approxeq_id_Double :: Property
prop_inverse_times_approxeq_id_Double =
  squareMatrixTestDouble inverse_times_approxeq_id

prop_inverse_times_eq_id_Rational :: Property
prop_inverse_times_eq_id_Rational = squareMatrixTestRational inverse_times_eq_id

return []
allIndexedMatrixTests :: IO Bool
allIndexedMatrixTests = $(quickCheckAll)

