{-# LANGUAGE RankNTypes
  , ScopedTypeVariables
  , TemplateHaskell
  #-}
module Test.ValueSimplex where
import Prelude hiding (all)
import Control.Applicative ((<$>), (<*>))
import Data.Foldable (all)
import Data.Function (on)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromJust, isNothing)
import Data.Ratio ((%))
import Data.Set (Set)
import qualified Data.Set as Set
import Numeric.Matrix (MatrixElement)
import Test.QuickCheck
import Test.QuickCheck.All
import Test.Set
import Util.ApproxEq
import Util.Function ((.!), (....))
import Util.Monad ((>>=*))
import Util.Set (distinctPairs, distinctPairsOneWay)
import ValueSimplex

--------------------------------------------------------------------------------

instance Fractional a => Fractional (Positive a) where
  Positive x / Positive y = Positive $ x / y
  recip (Positive x) = Positive $ recip x
  fromRational = Positive . fromRational

liftEquivToValueSimplex :: (Ord a, Num b) =>
  (b -> b -> Bool) -> ValueSimplex a b -> ValueSimplex a b -> Bool
liftEquivToValueSimplex eq vs vs' = let xs = nodes vs
    in nodes vs' == xs
    &&  ( flip all (distinctPairs xs) $ \(x, y) ->
            vsLookup vs x y `eq` vsLookup vs' x y
        )

instance (Ord a, Num b, RelApproxEq b) => RelApproxEq (ValueSimplex a b) where
  (~~=) = liftEquivToValueSimplex (~~=)

validify :: (Ord a, Fractional b) => a -> (a -> a -> b) -> a -> a -> b
validify x f y z =
  if y < z || y == x || z == x
  then f y z
  else f z y * f y x * f x z / (f z x * f x y)

instance
  (Arbitrary a, CoArbitrary a, Ord a, Arbitrary b, Ord b, Fractional b) =>
  Arbitrary (ValueSimplex a b) where
  arbitrary = do
    x <- arbitrary
    f <- arbitrary
    xs <- arbitrarySetOfSizeSqrtOrMin 2
    return $ fromFunction (validify x $ getPositive .! f) xs

--------------------------------------------------------------------------------

withArbitraryNode :: (Ord a, Show a, Testable p)
  => ValueSimplex a b -> (a -> p) -> Property
withArbitraryNode = withArbitraryElement . nodes

with2ArbitraryNodes :: (Ord a, Show a, Testable p)
  => ValueSimplex a b -> (a -> a -> p) -> Property
with2ArbitraryNodes = with2ArbitraryElements . nodes

testList :: Testable p => [a -> p] -> a -> Property
testList ts x = foldr (.&&.) (property True) $ map ($ x) ts

--------------------------------------------------------------------------------

vsLookup_fromFunction ::
  (Arbitrary a, CoArbitrary a, Ord a, Show a, Arbitrary b, Eq b, Num b) =>
  Blind (a -> a -> b) -> Property
vsLookup_fromFunction (Blind f) = forAll arbitrarySetOfSizeSqrt $ \xs ->
    with2ArbitraryElements xs $ \x y ->
      vsLookup (fromFunction f xs) x y == f x y

prop_vsLookup_fromFunction_Double ::
  (Arbitrary a, CoArbitrary a, Ord a, Show a) =>
  Blind (a -> a -> Double) -> Property
prop_vsLookup_fromFunction_Double = vsLookup_fromFunction

nodes_fromFunction ::
  (Arbitrary a, CoArbitrary a, Ord a, Show a, Arbitrary b) =>
  Blind (a -> a -> b) -> Property
nodes_fromFunction (Blind f) = forAll (arbitrarySetOfSizeSqrtOrMin 2) $
  \xs -> nodes (fromFunction f xs) == xs

prop_nodes_fromFunction_Double :: (Arbitrary a, CoArbitrary a, Ord a, Show a)
  => Blind (a -> a -> Double) -> Property
prop_nodes_fromFunction_Double = nodes_fromFunction

prop_empty :: (Arbitrary a, CoArbitrary a, Ord a, Show a)
  => Blind (a -> a -> Double) -> Bool
prop_empty (Blind f) = isEmpty $ fromFunction f Set.empty

--------------------------------------------------------------------------------

fromFunction_vsLookup :: (Ord a, Eq b, Num b) => ValueSimplex a b -> Bool
fromFunction_vsLookup vs = fromFunction (vsLookup vs) (nodes vs) == vs

values_positive :: (Ord a, Show a, Ord b, Num b) => ValueSimplex a b -> Property
values_positive vs = with2ArbitraryNodes vs $ (0 <) .! vsLookup vs

non_degenerate :: Ord a => ValueSimplex a b -> Bool
non_degenerate = (2 <=) . Set.size . nodes

exact_ValueSimplex_validity_tests :: (Ord a, Show a, Ord b, Num b)
  => ValueSimplex a b -> Property
exact_ValueSimplex_validity_tests = testList
  [ printTestCase "fromFunction_vsLookup" . fromFunction_vsLookup
  , printTestCase "values_positive" . values_positive
  , printTestCase "non_degenerate" . non_degenerate
  , printTestCase "non-empty" . not . isEmpty
  ]

prop_exact_ValueSimplex_validity_tests_Double :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_exact_ValueSimplex_validity_tests_Double =
  exact_ValueSimplex_validity_tests

--------------------------------------------------------------------------------

approx_Double_exact_Rational ::
  forall t. Testable t =>
  (forall b. (Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b) =>
      (b -> b -> Bool) -> ValueSimplex Integer b -> t) ->
    Property
approx_Double_exact_Rational test =
  (test (~~=) :: ValueSimplex Integer Double -> t)
  .&&.  (test (==) :: ValueSimplex Integer Rational -> t)

ok :: (Ord a, Ord b, Num b) => (b -> b -> Bool) -> ValueSimplex a b -> Bool
ok = (OK ==) .! status

could_have_been_arbitrary :: (Ord a, Show a, Fractional b) =>
  (b -> b -> Bool) -> ValueSimplex a b -> Property
could_have_been_arbitrary eq vs = let xs = nodes vs in
  withArbitraryNode vs $ \x ->
    liftEquivToValueSimplex eq vs $ fromFunction (validify x $ vsLookup vs) xs

approx_ValueSimplex_validity_tests :: (Ord a, Show a, Ord b, Fractional b)
  => (b -> b -> Bool) -> ValueSimplex a b -> Property
approx_ValueSimplex_validity_tests = curry $ testList $ map uncurry
  [ printTestCase "ok" .! ok
  , printTestCase "could_have_been_arbitrary" .! could_have_been_arbitrary
  ]

prop_approx_ValueSimplex_validity_tests :: Property
prop_approx_ValueSimplex_validity_tests =
  approx_Double_exact_Rational approx_ValueSimplex_validity_tests

--------------------------------------------------------------------------------

prop_price_self :: (Ord a, Show a) => ValueSimplex a Double -> Property
prop_price_self vs = withArbitraryNode vs $ \x -> price vs x x == 1

compoundPrice :: (Ord a, Eq b, Fractional b)
  => ValueSimplex a b -> a -> a -> a -> b
compoundPrice vs x y z = ((*) `on` uncurry (price vs)) (x, y) (y, z)

equal_values :: (Ord a, Show a, Eq b, Fractional b) =>
  (b -> b -> Bool) -> ValueSimplex a b -> Property
equal_values eq vs = with2ArbitraryNodes vs $ \x y ->
  (vsLookup vs x y * price vs x y) `eq` vsLookup vs y x

prop_equal_values :: Property
prop_equal_values = approx_Double_exact_Rational equal_values

prices_reciprocal :: (Ord a, Show a, Eq b, Fractional b) =>
  (b -> b -> Bool) -> ValueSimplex a b -> Property
prices_reciprocal eq vs = with2ArbitraryNodes vs $ \x y ->
  compoundPrice vs x y x `eq` 1

prop_prices_reciprocal :: Property
prop_prices_reciprocal = approx_Double_exact_Rational prices_reciprocal

price_cycle :: (Ord a, Show a, Eq b, Fractional b) =>
  (b -> b -> Bool) -> ValueSimplex a b -> Property
price_cycle eq vs = with3ArbitraryElements (nodes vs) $ \x y z ->
  compoundPrice vs x y z `eq` price vs x z

prop_price_cycle :: Property
prop_price_cycle = approx_Double_exact_Rational price_cycle

prop_hybridPrice_correct :: (Ord a, Show a) => ValueSimplex a Double -> Property
prop_hybridPrice_correct vs = with2ArbitraryNodes vs $ \x y ->
  withArbitraryNode vs $ \z ->
  (hybridPrice vs x y z) ^ 2 ~~= price vs x z * price vs y z

--------------------------------------------------------------------------------

nodeValue_correct :: (Ord a, Show a, Num b)
  => (b -> b -> Bool) -> ValueSimplex a b -> Property
nodeValue_correct eq vs = let xs = nodes vs in
  withArbitraryElementAndRest xs $ \x xs' ->
    nodeValue vs x `eq` Set.foldr ((+) . vsLookup vs x) 0 xs'

prop_nodeValue_correct :: Property
prop_nodeValue_correct = approx_Double_exact_Rational nodeValue_correct

linkValueSquared_correct :: (Ord a, Show a, Eq b, Num b)
  => ValueSimplex a b -> Property
linkValueSquared_correct vs = with2ArbitraryNodes vs $ \x y ->
  linkValueSquared vs x y == vsLookup vs x y * vsLookup vs y x

prop_linkValueSquared_correct_Double :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_linkValueSquared_correct_Double = linkValueSquared_correct

prop_halfLinkValue_correct :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_halfLinkValue_correct vs = with2ArbitraryNodes vs $ \x y ->
  (halfLinkValue vs x y) ^ 2 ~~= linkValueSquared vs x y

--------------------------------------------------------------------------------

sell_too_much ::
  (Ord a, Show a, Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> Property
sell_too_much vs = with2ArbitraryNodes vs $ \x0 x1 ->
  forAll (arbitrary `suchThat` (<= - supremumSellable vs x0 x1)) $ \q0 ->
  property $ \q1 -> vsLookup (update vs x0 q0 x1 q1) x0 x1 <= 0

prop_sell_too_much_Double :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_sell_too_much_Double = sell_too_much

arbitrarySellable ::
  (Ord a, Show a, Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> a -> a -> Gen b
arbitrarySellable vs x0 x1 =
  arbitrary `suchThat` (> - supremumSellable vs x0 x1)

withSomeArbitraryUpdateParameters ::
  ( Ord a, Show a
  , Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b
  , Testable p
  )
  => ValueSimplex a b -> (a -> b -> a -> p) -> Property
withSomeArbitraryUpdateParameters vs t = with2ArbitraryNodes vs $ \x0 x1 ->
  forAll (arbitrarySellable vs x0 x1) $ flip (t x0) x1

breakEven_sellable ::
  (Ord a, Show a, Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> Property
breakEven_sellable vs = withSomeArbitraryUpdateParameters vs $ \x0 q0 x1 ->
  breakEven vs x0 q0 x1 > - supremumSellable vs x1 x0

prop_breakEven_sellable_Double :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_breakEven_sellable_Double = breakEven_sellable

update_symmetric ::
  (Ord a, Show a, Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b)
  => (b -> b -> Bool) -> ValueSimplex a b -> Property
update_symmetric eq vs = with2ArbitraryNodes vs $ \x0 x1 -> property $ \q0 q1 ->
  liftEquivToValueSimplex eq (update vs x0 q0 x1 q1) (update vs x1 q1 x0 q0)

prop_update_symmetric :: Property
prop_update_symmetric = approx_Double_exact_Rational update_symmetric

prop_update_zero :: Property
prop_update_zero = approx_Double_exact_Rational $ \eq vs ->
  with2ArbitraryNodes vs $ \x0 x1 ->
  liftEquivToValueSimplex eq vs $ update vs x0 0 x1 0

withArbitraryUpdateParameters ::
  ( Ord a, Show a
  , Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b
  , Testable p
  )
  => ValueSimplex a b -> (a -> b -> a -> b -> p) -> Property
withArbitraryUpdateParameters vs t =
  withSomeArbitraryUpdateParameters vs $ \x0 q0 x1 ->
  forAll (arbitrarySellable vs x1 x0) $ t x0 q0 x1

prop_update_exact_ValueSimplex_validity_tests_Double :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_update_exact_ValueSimplex_validity_tests_Double vs =
  withArbitraryUpdateParameters vs $
  exact_ValueSimplex_validity_tests .... update vs

prop_update_approx_ValueSimplex_validity_tests :: Property
prop_update_approx_ValueSimplex_validity_tests =
  approx_Double_exact_Rational $ \eq vs ->
  withArbitraryUpdateParameters vs $
  approx_ValueSimplex_validity_tests eq .... update vs

prop_update_nodes_unchanged_Double :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_update_nodes_unchanged_Double vs = withArbitraryUpdateParameters vs $
  ((nodes vs ==) . nodes) .... update vs

prop_update_changed_nodeValue :: Property
prop_update_changed_nodeValue = approx_Double_exact_Rational $ \eq vs ->
  withArbitraryUpdateParameters vs $ \x0 q0 x1 q1 ->
  let vs' = update vs x0 q0 x1 q1 in
  nodeValue vs' x0 `eq` (nodeValue vs x0 + q0)
  && nodeValue vs' x1 `eq` (nodeValue vs x1 + q1)

prop_update_unchanged_nodeValue :: Property
prop_update_unchanged_nodeValue = approx_Double_exact_Rational $ \eq vs ->
  withArbitraryUpdateParameters vs $ \x0 q0 x1 q1 ->
  withArbitraryElement (Set.difference (nodes vs) $ Set.fromList [x0, x1]) $
  \x -> nodeValue (update vs x0 q0 x1 q1) x `eq` nodeValue vs x

prop_uniform_value_change :: Property
prop_uniform_value_change = approx_Double_exact_Rational $ \eq vs ->
  withArbitraryUpdateParameters vs $ \x0 q0 x1 q1 ->
  with2ArbitraryNodes vs $ \x y ->
  let
    ss = linkValueSquared vs
    ss' = linkValueSquared $ update vs x0 q0 x1 q1
  in
  ss' x y `eq` ss x y
  || compare (ss' x y) (ss x y) == compare (ss' x0 x1) (ss x0 x1)

prop_profit_Double :: (Ord a, Show a) => ValueSimplex a Double -> Property
prop_profit_Double vs = withSomeArbitraryUpdateParameters vs $ \x0 q0 x1 ->
  forAll (arbitrary `suchThat` (> breakEven vs x0 q0 x1)) $ \q1 ->
  linkValueSquared (update vs x0 q0 x1 q1) x0 x1 > linkValueSquared vs x0 x1

prop_breakEven :: Property
prop_breakEven = approx_Double_exact_Rational $ \eq vs ->
  withSomeArbitraryUpdateParameters vs $ \x0 q0 x1 ->
  linkValueSquared (update vs x0 q0 x1 (breakEven vs x0 q0 x1)) x0 x1
  `eq` linkValueSquared vs x0 x1

prop_loss_Double :: (Ord a, Show a) => ValueSimplex a Double -> Property
prop_loss_Double vs = withSomeArbitraryUpdateParameters vs $ \x0 q0 x1 ->
  forAll (choose (- supremumSellable vs x1 x0, breakEven vs x0 q0 x1)) $ \q1 ->
  linkValueSquared (update vs x0 q0 x1 q1) x0 x1 < linkValueSquared vs x0 x1

arbitrarilyUpdatedValueSimplex ::
  (Ord a, Show a, Arbitrary b, Ord b, Show b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> Gen (ValueSimplex a b)
arbitrarilyUpdatedValueSimplex vs = let xs = nodes vs in
  if Set.size xs < 2
  then return vs
  else do
    x0 <- elements $ Set.toList xs
    x1 <- elements $ Set.toList $ Set.delete x0 xs
    q0 <- arbitrarySellable vs x0 x1
    q1 <- arbitrarySellable vs x1 x0
    return $ update vs x0 q0 x1 q1

longrun_validity :: (Ord a, Show a)
  => (ValueSimplex a Double -> Gen (ValueSimplex a Double))
  -> ValueSimplex a Double -> Property
longrun_validity upd vs =
  sized $ \n -> do
    k <- choose (0, n)
    forAll
      (vs >>=* replicate k upd)
      $ approx_ValueSimplex_validity_tests (~~=)

prop_longrun_validity :: (Ord a, Show a) => ValueSimplex a Double -> Property
prop_longrun_validity = longrun_validity arbitrarilyUpdatedValueSimplex

--------------------------------------------------------------------------------

prop_multiUpdate_nodes_unchanged :: Ord a
  => ValueSimplex a Double -> Blind (a -> Double) -> Bool
prop_multiUpdate_nodes_unchanged vs (Blind f) =
  nodes (multiUpdate vs f) == nodes vs

prop_multiUpdate_nodeValue :: Property
prop_multiUpdate_nodeValue = approx_Double_exact_Rational $ \eq vs ->
  property $ \(Blind f') -> let f = getPositive . f' in
  withArbitraryNode vs $
  eq <$> nodeValue (multiUpdate vs f) <*> f

prop_multiUpdate_uniform_value_change :: Property
prop_multiUpdate_uniform_value_change = approx_Double_exact_Rational $ \eq vs ->
  property $ \(Blind f) ->
  with2ArbitraryNodes vs $ \x y ->
  with2ArbitraryNodes vs $ \i j ->
  let
    ss = linkValueSquared vs
    ss' = linkValueSquared $ multiUpdate vs $ getPositive . f
    ssxy = ss x y
    ssij = ss i j
    ss'xy = ss' x y
    ss'ij = ss' i j
  in
  compare ss'xy ssxy == compare ss'ij ssij || ss'xy `eq` ssxy || ss'ij `eq` ssij

multiUpdateWithAdjustmentList :: (Ord a, Ord b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> [(a, b)] -> ValueSimplex a b
multiUpdateWithAdjustmentList vs xqs = multiUpdate vs $ \x ->
  nodeValue vs x + Map.findWithDefault 0 x (Map.fromListWith (+) xqs)

generated_multiUpdate_improving :: (Ord a, Show a)
  => ValueSimplex a Double
  -> (Set a -> Set (a, a))
  -> (a -> a -> Gen (Double, Double))
  -> Property
generated_multiUpdate_improving vs pairf qgen =
  forAll (arbitrarySubset $ pairf $ nodes vs) $ \xxs ->
  forAll
    (promote $ flip map (Set.toList xxs) $ \(x0, x1) -> do
      (q0, q1) <- qgen x0 x1
      return [(x0, q0), (x1, q1)]
    ) $ \xqs ->
  with2ArbitraryNodes vs $ \x y ->
  linkValueSquared vs x y <=
  linkValueSquared (multiUpdateWithAdjustmentList vs $ concat xqs) x y

prop_multiUpdate_improving :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_multiUpdate_improving vs =
  let
    s = vsLookup vs
    ss = linkValueSquared vs
  in
  generated_multiUpdate_improving vs distinctPairsOneWay $ \x0 x1 -> do
    q0 <- arbitrary `suchThat` (> - s x0 x1)
    q1 <- arbitrary `suchThat`
      (\q -> (s x0 x1 + q0) * (s x1 x0 + q) >= ss x0 x1)
    return (q0, q1)

prop_multiUpdate_exact_validity_tests :: (Ord a, Show a)
  => ValueSimplex a Double -> Blind (a -> Positive Double) -> Property
prop_multiUpdate_exact_validity_tests vs (Blind f) =
  prop_exact_ValueSimplex_validity_tests_Double $
    multiUpdate vs $ getPositive . f

prop_multiUpdate_approx_validity_tests :: Property
prop_multiUpdate_approx_validity_tests =
  approx_Double_exact_Rational $ \eq vs ->
  property $ \(Blind f) ->
  approx_ValueSimplex_validity_tests eq $ multiUpdate vs $ getPositive . f

--------------------------------------------------------------------------------

prop_linkOptimumAtPrice_optimum :: (Ord a, Show a) =>
  ValueSimplex a Double -> Positive Double -> Double -> Property
prop_linkOptimumAtPrice_optimum vs (Positive p) q =
  with2ArbitraryNodes vs $ \x0 x1 ->
  let
    s = vsLookup vs
    ss' q0 q1 = (s x0 x1 + q0) * (s x1 x0 + q1)
  in
  ss' q (-p * q) <= uncurry ss' (linkOptimumAtPrice vs x0 x1 p)

prop_linkOptimumAtPrice_at_price :: Property
prop_linkOptimumAtPrice_at_price = approx_Double_exact_Rational $ \eq vs ->
  with2ArbitraryNodes vs $ \x0 x1 ->
  property $ \(Positive p) ->
  let (q0, q1) = linkOptimumAtPrice vs x0 x1 p in
  q1 `eq` (-p * q0)

prop_linkOptimumAtPrice_valid :: (Ord a, Show a)
  => ValueSimplex a Double -> Positive Double -> Property
prop_linkOptimumAtPrice_valid vs (Positive p) =
  with2ArbitraryNodes vs $ \x0 x1 ->
  let
    (q0, q1) = linkOptimumAtPrice vs x0 x1 p
    s = vsLookup vs
  in
  q0 > - s x0 x1 && q1 > - s x1 x0

prop_linkOptimumAtPrice_correct_sign :: (Ord a, Show a)
  => ValueSimplex a Double -> Positive Double -> Property
prop_linkOptimumAtPrice_correct_sign vs (Positive p) =
  with2ArbitraryNodes vs $ \x0 x1 ->
  compare p (price vs x0 x1)
  == compare 0 (fst $ linkOptimumAtPrice vs x0 x1 p)

prop_multiUpdate_linkOptimumAtPrice_improving :: (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_multiUpdate_linkOptimumAtPrice_improving vs =
  generated_multiUpdate_improving vs distinctPairs $ \x0 x1 -> do
    p <- arbitrary `suchThat` (>= price vs x0 x1)
    return $ linkOptimumAtPrice vs x0 x1 p

arbitrarilyMultiUpdatedWithLinkOptimumAtNearPrice ::
  (Ord a, Ord b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> Gen (ValueSimplex a b)
arbitrarilyMultiUpdatedWithLinkOptimumAtNearPrice vs = do
  let xs = nodes vs
  xxs <- arbitrarySubset $ distinctPairs xs
  return $ multiUpdateWithAdjustmentList vs $ concat $ flip map (Set.toList xxs)
    $ \(x0, x1) ->
      let
        p = 1.01 * price vs x0 x1
        (q0, q1) = linkOptimumAtPrice vs x0 x1 p
      in
      [(x0, q0), (x1, q1)]

prop_multiUpdate_linkOptimumAtNearPrice_longrun_validity ::
  (Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_multiUpdate_linkOptimumAtNearPrice_longrun_validity =
  longrun_validity arbitrarilyMultiUpdatedWithLinkOptimumAtNearPrice

--------------------------------------------------------------------------------

prop_totalValue_correct :: Property
prop_totalValue_correct = approx_Double_exact_Rational $ \eq vs ->
  withArbitraryNode vs $ \x ->
  totalValue vs x 
  `eq` Set.foldr ((+) . (\y -> nodeValue vs y * price vs y x)) 0 (nodes vs)

arbitraryFractionalBetween0and1 :: Fractional a => Gen a
arbitraryFractionalBetween0and1 = let precision = 9999999999999 in
  do
    b <- choose (2, precision)
    a <- choose (1, b - 1)
    return $ fromRational $ a % b

withArbitraryAddNodeParameters ::
  ( Arbitrary a, Ord a, Show a
  , Arbitrary b, Ord b, Fractional b, Show b
  , Testable p
  )
  => ValueSimplex a b -> (a -> b -> a -> b -> p) -> Property
withArbitraryAddNodeParameters vs test =
  property $ \x ->
  Set.notMember x (nodes vs) ==>
  property $ \(Positive q) ->
  withArbitraryNode vs $ \y ->
  forAll arbitraryFractionalBetween0and1 $ \r ->
  test x q y $ r * totalValue vs y / q

withArbitrarilyAddNodedValueSimplex ::
  ( Arbitrary a, Ord a, Show a
  , Arbitrary b, Ord b, Fractional b, Show b
  , Testable p
  )
  => ValueSimplex a b -> (ValueSimplex a b -> p) -> Property
withArbitrarilyAddNodedValueSimplex vs test =
  withArbitraryAddNodeParameters vs $ test .... addNode vs

testArbitrarilyAddNodedValueSimplex ::
  ( Arbitrary a, Ord a, Show a
  , Arbitrary b, Ord b, Fractional b, Show b
  , Testable p
  )
  => (ValueSimplex a b -> p) -> ValueSimplex a b -> Property
testArbitrarilyAddNodedValueSimplex = flip withArbitrarilyAddNodedValueSimplex

prop_addNode_exact_validity_tests :: (Arbitrary a, Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_addNode_exact_validity_tests =
  testArbitrarilyAddNodedValueSimplex exact_ValueSimplex_validity_tests

prop_addNode_approx_validity_tests :: Property
prop_addNode_approx_validity_tests =
  approx_Double_exact_Rational $
  testArbitrarilyAddNodedValueSimplex . approx_ValueSimplex_validity_tests

prop_addNode_nodes :: (Arbitrary a, Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_addNode_nodes vs = withArbitraryAddNodeParameters vs $ \x q y p ->
  nodes (addNode vs x q y p) == Set.insert x (nodes vs)

prop_addNode_nodeValue :: Property
prop_addNode_nodeValue = approx_Double_exact_Rational $ \eq vs ->
  withArbitraryAddNodeParameters vs $ \x q y p ->
  nodeValue (addNode vs x q y p) x `eq` q

prop_addNode_price :: Property
prop_addNode_price =
  approx_Double_exact_Rational $ \eq vs ->
  withArbitraryAddNodeParameters vs $ \x q y p ->
  price (addNode vs x q y p) x y `eq` p

prop_addNode_nodeValue_unchanged :: Property
prop_addNode_nodeValue_unchanged =
  approx_Double_exact_Rational $ \eq vs ->
  withArbitrarilyAddNodedValueSimplex vs $ \vs' ->
  withArbitraryNode vs $ \i ->
  nodeValue vs' i `eq` nodeValue vs i

prop_addNode_price_unchanged :: Property
prop_addNode_price_unchanged =
  approx_Double_exact_Rational $ \eq vs ->
  withArbitrarilyAddNodedValueSimplex vs $ \vs' ->
  with2ArbitraryNodes vs $ \i j ->
  price vs' i j `eq` price vs i j

prop_addNode_proportions_unchanged :: Property
prop_addNode_proportions_unchanged =
  approx_Double_exact_Rational $ \eq vs ->
  let ss = linkValueSquared vs in
  withArbitrarilyAddNodedValueSimplex vs $ \vs' ->
  let ss' = linkValueSquared vs' in
  with2ArbitraryNodes vs $ \i j ->
  with2ArbitraryNodes vs $ \k l ->
  (ss' i j / ss' k l) `eq` (ss i j / ss k l)

--------------------------------------------------------------------------------

prop_strictlySuperior_irreflexive :: Property
prop_strictlySuperior_irreflexive =
  approx_Double_exact_Rational $ \eq vs ->
  not $ strictlySuperior eq vs vs

prop_multiUpdate_strictlySuperior_one_way :: Property
prop_multiUpdate_strictlySuperior_one_way =
  approx_Double_exact_Rational $ \eq vs ->
  property $ \(Blind f) ->
  let vs' = multiUpdate vs $ getPositive . f in
  not (liftEquivToValueSimplex eq vs vs') ==>
  strictlySuperior eq vs vs' == not (strictlySuperior eq vs' vs)

prop_multiUpdate_linkOptimumAtNearPrice_strictlySuperior :: Property
prop_multiUpdate_linkOptimumAtNearPrice_strictlySuperior =
  approx_Double_exact_Rational $ \eq vs -> property $ do
  vs' <- arbitrarilyMultiUpdatedWithLinkOptimumAtNearPrice vs
  return $ not (liftEquivToValueSimplex eq vs vs') ==>
    strictlySuperior eq vs' vs

--------------------------------------------------------------------------------

arbitraryDepositFunction :: (CoArbitrary a, Ord a, Arbitrary b, Ord b, Num b)
  => ValueSimplex a b -> Gen (a -> b)
arbitraryDepositFunction vs = do
  xs <- arbitrarySubset $ nodes vs
  f <- (getPositive .) <$> arbitrary
  return $ \x ->
    if Set.member x xs
      then nodeValue vs x + f x
      else nodeValue vs x

arbitrarilyDeposited ::
  (CoArbitrary a, Ord a)
  => ValueSimplex a Double -> Gen (ValueSimplex a Double)
arbitrarilyDeposited vs = deposit vs <$> arbitraryDepositFunction vs

prop_arbitrarilyDeposited_validity_tests :: (CoArbitrary a, Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_arbitrarilyDeposited_validity_tests vs = do
  vs' <- arbitrarilyDeposited vs
  exact_ValueSimplex_validity_tests vs'
    .&&. approx_ValueSimplex_validity_tests (~~=) vs'

prop_arbitrarilyDeposited_nodes_unchanged :: (CoArbitrary a, Ord a)
  => ValueSimplex a Double -> Property
prop_arbitrarilyDeposited_nodes_unchanged vs = property $
  (nodes vs ==) . nodes <$> arbitrarilyDeposited vs

prop_deposit_nodeValue :: (CoArbitrary a, Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_deposit_nodeValue vs = do
  f <- arbitraryDepositFunction vs
  withArbitraryNode vs $ \x -> nodeValue (deposit vs f) x ~~= f x

prop_deposit_valueEvenly :: (CoArbitrary a, Ord a, Show a)
  => ValueSimplex a Double -> Property
prop_deposit_valueEvenly vs = forAll (arbitrarilyDeposited vs) $ \vs' ->
  let v = halfLinkValue vs
      v' = halfLinkValue vs' in
  with2ArbitraryNodes vs $ \x y -> with2ArbitraryNodes vs $ \z w ->
    let l = (v' x y - v x y) * hybridPrice vs x y x
        r = (v' z w - v z w) * hybridPrice vs z w x in
    printTestCase (show l ++ "\n" ++ show r) $ l ~~= r

--------------------------------------------------------------------------------

prop_linkBreakEvenAtPriceWithFee_impossible :: (Ord a, Show a)
  => ValueSimplex a Double -> Positive Double -> Double -> Property
prop_linkBreakEvenAtPriceWithFee_impossible vs (Positive p) q =
  let s = vsLookup vs in
  with2ArbitraryNodes vs $ \x y ->
  forAll
    (arbitrary `suchThat` (isNothing . linkBreakEvenAtPriceWithFee vs x y p))
    $ \f -> (s x y - q / p) * (s y x + q - f) < s x y * s y x

prop_linkBreakEvenAtPriceWithFee_possible :: (Ord a, Show a)
  => ValueSimplex a Double -> Positive Double -> Double -> Positive Double
  -> Property
prop_linkBreakEvenAtPriceWithFee_possible vs (Positive p) f (Positive qd) =
  with2ArbitraryNodes vs $ \x y ->
  case linkBreakEvenAtPriceWithFee vs x y p f of
    Nothing -> discard
    Just ((qx, qy), (qx', qy')) ->
      let sx = vsLookup vs x y
          sy = vsLookup vs y x
          sxsy = sx * sy
          testq q = (sx - q / p) * (sy + q - f)
      in
      (     and
              [ testq qy  ~~= sxsy
              , testq qy' ~~= sxsy
              , qy  ~~= -p * qx
              , qy' ~~= -p * qx'
              , qy < qy'
              , testq (qy  - qd) < sxsy
              , testq (qy' + qd) < sxsy
              ]     )
      .&&.  forAll (choose (qy, qy')) (\qym -> testq qym > sxsy)

--------------------------------------------------------------------------------

return []
allValueSimplexTests :: IO Bool
allValueSimplexTests = $(quickCheckAll)

