{-# LANGUAGE TemplateHaskell #-}

module Test.Monotonic
  ( allMonotonicTests
  ) where

import Data.Word (Word64)
import Test.QuickCheck (Property, arbitraryBoundedIntegral, forAll)
import Test.QuickCheck.All (quickCheckAll)
import Util.Monotonic (monotonicWordToDouble, monotonicDoubleToWord)

prop_monotonicWordToDouble_monotonic :: Word64 -> Word64 -> Bool
prop_monotonicWordToDouble_monotonic x y =
  let c = compare (monotonicWordToDouble x) (monotonicWordToDouble y) in
  c == EQ || c == compare x y

prop_monotonicWordToDouble_fullRange_monotonic :: Property
prop_monotonicWordToDouble_fullRange_monotonic = forAll arbitraryBoundedIntegral
  $ forAll arbitraryBoundedIntegral . prop_monotonicWordToDouble_monotonic

prop_monotonicWordToDouble_monotonicDoubleToWord :: Double -> Bool
prop_monotonicWordToDouble_monotonicDoubleToWord x =
  monotonicWordToDouble (monotonicDoubleToWord x) == x

prop_monotonicDoubleToWord_strictlyMonotonic :: Double -> Double -> Bool
prop_monotonicDoubleToWord_strictlyMonotonic x y =
  compare (monotonicDoubleToWord x) (monotonicDoubleToWord y) == compare x y

return []
allMonotonicTests :: IO Bool
allMonotonicTests = $(quickCheckAll)

