module Test.Set where
import Control.Monad (liftM)
import Data.List (find)
import Data.Maybe (fromJust)
import Data.Set (Set)
import qualified Data.Set as Set
import Test.QuickCheck
import Util.Function ((.!))

arbitrarySetOfExactSize :: (Arbitrary a, Ord a) => Int -> Gen (Set a)
arbitrarySetOfExactSize n = liftM
  ( fromJust
  . find ((n ==) . Set.size)
  . (scanl (flip Set.insert) Set.empty)
  )
  $ sequence $ repeat arbitrary

arbitrarySetOfSizeSqrt :: (Arbitrary a, Ord a) => Gen (Set a)
arbitrarySetOfSizeSqrt = sized $ \n -> do
  k <- choose (0, ceiling $ sqrt $ realToFrac n)
  liftM Set.fromList $ sequence $ replicate k arbitrary

incrementSize :: Gen a -> Gen a
incrementSize x = sized $ \n -> resize (n+1) x

arbitrarySetOfSizeSqrtOrMin :: (Arbitrary a, Ord a) => Int -> Gen (Set a)
arbitrarySetOfSizeSqrtOrMin n = do
  xs <- arbitrarySetOfSizeSqrt
  if Set.size xs < n
  then incrementSize $ arbitrarySetOfSizeSqrtOrMin n
  else return xs

arbitrarySubset :: Ord a => Set a -> Gen (Set a)
arbitrarySubset xs = liftM (Set.fromList . map fst . filter snd) $
  flip mapM (Set.toList xs) $ \x -> do
    keep <- arbitrary
    return (x, keep)

withArbitraryElementAndRest :: (Ord a, Show a, Testable p) =>
  Set a -> (a -> Set a -> p) -> Property
withArbitraryElementAndRest xs f = not (Set.null xs) ==>
  forAll (elements $ Set.toList xs) $ \x -> f x $ Set.delete x xs

withAnotherArbitraryElement :: (Ord a, Show a) =>
  (Set a -> b -> Property) -> Set a -> (a -> b) -> Property
withAnotherArbitraryElement t xs f = withArbitraryElementAndRest xs $ \x xs' ->
  t xs' $ f x

withArbitraryElement :: (Ord a, Show a, Testable p) =>
  Set a -> (a -> p) -> Property
withArbitraryElement = withAnotherArbitraryElement $ property .! flip const

with2ArbitraryElements :: (Ord a, Show a, Testable p) =>
  Set a -> (a -> a -> p) -> Property
with2ArbitraryElements = withAnotherArbitraryElement withArbitraryElement

with3ArbitraryElements :: (Ord a, Show a, Testable p) =>
  Set a -> (a -> a -> a -> p) -> Property
with3ArbitraryElements = withAnotherArbitraryElement with2ArbitraryElements

