module Main where
import Control.Applicative ((<$>))
import Data.List (intercalate)
import Data.Set (Set)
import qualified Data.Set as Set
import Fund (Fund(XRP), btcBitstamp, usdBitstamp, cnyRippleCN)
import Numeric.Matrix (MatrixElement)
import System.Exit (exitSuccess)
import Test.QuickCheck (sample')
import Test.ValueSimplex (arbitrarilyMultiUpdatedWithLinkOptimumAtNearPrice)
import Util.Foldable (sumWith)
import Util.Monad ((>>=*))
import Util.Set (distinctTriplesOneWay)
import ValueSimplex

arbitrageability :: (Ord a, Floating b) => ValueSimplex a b -> b
arbitrageability vs =
  flip sumWith (distinctTriplesOneWay $ nodes vs) $ \(x, y, z) ->
  let s = vsLookup vs in
  abs $ log $ s x y * s y z * s z x / (s z y * s y x * s x z)

printAndUpdate :: (Ord b, Floating b, MatrixElement b, Show b)
  => ValueSimplex Fund b -> IO (ValueSimplex Fund b)
printAndUpdate vs = do
  putStrLn $ intercalate "," $ map show
    [ arbitrageability vs
    , nodeValue vs XRP
    , linkValueSquared vs XRP btcBitstamp
    ]
  head <$> (sample' $ arbitrarilyMultiUpdatedWithLinkOptimumAtNearPrice vs)

main :: IO ()
main = do
  flip (>>=*) (replicate 100000 printAndUpdate) $ flip fromFunction
    (Set.fromList [XRP, btcBitstamp, usdBitstamp, cnyRippleCN])
    $ \left _ ->
    let
      total fund
        | fund == XRP         = 717270000 :: Double
        | fund == btcBitstamp = 0.0183 
        | fund == usdBitstamp = 22.22
        | fund == cnyRippleCN = 132.58
        | otherwise           = 0
    in
    total left / 3
  exitSuccess

