{-# LANGUAGE OverloadedStrings
  , TemplateHaskell
  #-}
module Fund where
import Data.Text (Text)
import Database.Persist.TH (derivePersistField)

data IOULine = IOULine
  { peerAccount   :: Text
  , lineCurrency  :: Text
  }
  deriving (Show, Eq, Ord, Read)

data Fund
  = XRP
  | IOUFund IOULine
  deriving (Eq, Ord, Read, Show)
derivePersistField "Fund"

bitStamp, rippleCN :: Text
bitStamp = "rvYAfWj5gh67oV6fW32ZzP3Aw4Eubs59B"
rippleCN = "rnuF96W4SZoCJmbHYBFoJZpR8eCaxNvekK"

btcBitstamp, usdBitstamp, cnyRippleCN :: Fund
btcBitstamp = IOUFund $ IOULine {peerAccount = bitStamp, lineCurrency = "BTC"}
usdBitstamp = IOUFund $ IOULine {peerAccount = bitStamp, lineCurrency = "USD"}
cnyRippleCN = IOUFund $ IOULine {peerAccount = rippleCN, lineCurrency = "CNY"}

