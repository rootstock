module Main where
import Control.Applicative ((<$>))
import System.Exit (exitFailure, exitSuccess)
import Test.IndexedMatrix (allIndexedMatrixTests)
import Test.Monotonic (allMonotonicTests)
import Test.ValueSimplex (allValueSimplexTests)

main :: IO ()
main = do
  success <- and <$> sequence
    [ allIndexedMatrixTests
    , allMonotonicTests
    , allValueSimplexTests
    ]
  if success
  then do
    putStrLn "All tests passed!"
    exitSuccess
  else do
    putStrLn "At least one test failed."
    exitFailure

