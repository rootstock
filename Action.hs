{-# LANGUAGE TemplateHaskell #-}
module Action where
import Database.Persist.TH (derivePersistField)

data Action
  = Running
  | InitialSetup
  | AddNode
  | Deposit
  deriving (Eq, Show, Read)
derivePersistField "Action"

