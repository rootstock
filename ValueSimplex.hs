module ValueSimplex where
import Prelude hiding (all, any)
import Control.Applicative ((<$>), (<*>))
import Data.Foldable (all, any, maximumBy)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe, fromJust)
import Data.Set (Set)
import qualified Data.Set as Set
import IndexedMatrix
import Numeric.Matrix (MatrixElement)
import Numeric.Search.Bounded (search)
import Util.Foldable (sumWith)
import Util.Function ((.!), (...))
import Util.Monotonic (monotonicWordToDouble)
import Util.Set (distinctPairs, distinctPairsOneWay)

newtype ValueSimplex a b = VS {vsMap :: Map (a, a) b} deriving (Eq, Show)

data VSStatus
  = OK
  | WrongPairs
  | NonPositive
  | CyclesWrong
  | Empty
  deriving (Show, Eq)

nodes :: Ord a => ValueSimplex a b -> Set a
nodes vs = Set.map fst $ Map.keysSet $ vsMap vs

isEmpty :: Ord a => ValueSimplex a b -> Bool
isEmpty = Set.null . nodes

pairUp :: a -> b -> (a, b)
pairUp = curry id

vsLookupMaybe :: Ord a => ValueSimplex a b -> a -> a -> Maybe b
vsLookupMaybe vs x y = Map.lookup (x, y) $ vsMap vs

vsLookup :: (Ord a, Num b) => ValueSimplex a b -> a -> a -> b
vsLookup = fromMaybe 0 ... vsLookupMaybe

fromFunction :: Ord a => (a -> a -> b) -> Set a -> ValueSimplex a b
fromFunction f xs = VS $ Map.fromSet (uncurry f) $ distinctPairs xs

validTriangle :: (Ord a, Num b) =>
  (b -> b -> Bool) -> ValueSimplex a b -> a -> a -> a -> Bool
validTriangle eq vs x y z =
  (vsLookup vs x y * vsLookup vs y z * vsLookup vs z x)
  `eq` (vsLookup vs z y * vsLookup vs y x * vsLookup vs x z)

status :: (Ord a, Ord b, Num b) =>
  (b -> b -> Bool) -> ValueSimplex a b -> VSStatus
status eq vs =
  let dPairs = distinctPairs $ nodes vs in
  if Map.keysSet (vsMap vs) /= dPairs
  then WrongPairs
  else if any ((<= 0) . (uncurry $ vsLookup vs)) dPairs
  then NonPositive
  else
  case Set.minView $ nodes vs of
    Nothing -> Empty
    Just (x, xs) ->
      if all (uncurry $ validTriangle eq vs x) $ distinctPairsOneWay xs
      then OK
      else CyclesWrong

-- price vs x y is the number of ys required to equal one x in value, according
-- to the internal state of the ValueSimplex vs
price :: (Ord a, Eq b, Fractional b) => ValueSimplex a b -> a -> a -> b
price vs x y
  | x == y      = 1
  | s x y == 0  = 0
  | otherwise   = s y x / s x y
  where s = vsLookup vs

hybridPrice :: (Ord a, Eq b, Floating b) => ValueSimplex a b -> a -> a -> a -> b
{- hybridPrice vs x y z is the value of one sqrt(x * y) in terms of z, according
  to vs -}
hybridPrice vs x y z = sqrt $ price vs x z * price vs y z

nodeValue :: (Ord a, Num b) => ValueSimplex a b -> a -> b
{- This might be faster if it was
  implemented in a more complicated way, maybe using Map.splitLookup,
  but I think it might be O(n log n) either way.
  -}
nodeValue vs x = sumWith (vsLookup vs x) $ Set.delete x $ nodes vs

linkValueSquared :: (Ord a, Num b) => ValueSimplex a b -> a -> a -> b
linkValueSquared vs x y = vsLookup vs x y * vsLookup vs y x

halfLinkValue :: (Ord a, Floating b) => ValueSimplex a b -> a -> a -> b
halfLinkValue = sqrt ... linkValueSquared

distributionProportions :: (Ord a, Fractional b, MatrixElement b)
  => ValueSimplex a b -> a -> a -> a -> b
distributionProportions vs x0 x1 = let xs = nodes vs in
  case
    inverse $ indexedMatrix (Set.delete x1 xs) (Set.delete x0 xs) $ \x y ->
      if x == y
      then - nodeValue vs x
      else vsLookup vs x y
    of
    Nothing -> error "ValueSimplex with non-invertible first minor matrix"
    Just m -> flip (at' m) x0

supremumSellable :: (Ord a, Fractional b, MatrixElement b)
  => ValueSimplex a b -> a -> a -> b
supremumSellable vs x0 x1 = recip $ distributionProportions vs x0 x1 x1

breakEven :: (Ord a, Fractional b, MatrixElement b)
  => ValueSimplex a b -> a -> b -> a -> b
breakEven vs x0 q0 x1 =
  let
    s = vsLookup vs
    qM = supremumSellable vs x0 x1
  in
  -q0 * (s x1 x0 / s x0 x1) * qM / (q0 + qM)

update :: (Ord a, Ord b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> a -> b -> a -> b -> ValueSimplex a b
{- Suppose the following all hold for some suitable approximate equality (~=):
    status (~=) vs == OK
    for each i in {0, 1}:
      xi is in nodes vs
      qMi == supremumSellable vs xi x(1-i)
      qi > -qMi
    vs' == update vs x0 q0 x1 q1
    ss == linkValueSquared vs
    ss' == linkValueSquared vs'
  Then the following should also hold (unless rounding errors have compounded a
  little too much):
    status (~=) vs' == OK
    nodes vs' == nodes vs
    for each i in {0, 1}:
      nodeValue vs' xi ~= nodeValue vs xi + qi
    for each x in nodes vs other than x0 and x1:
      nodeValue vs' x ~= nodeValue vs x
    for all distinct x and y in nodes vs:
      ss' x y ~= ss x y
      || compare (ss' x y) (ss x y) == compare (ss' x0 x1) (ss x0 x1)
  If it is additionally the case that
    q1 / s x1 x0 >= -q0 / s x0 x1 * qM0 / (q0 + qM0)
    where s = vsLookup vs
  then it should also be the case that for all distinct x and y in nodes vs,
    ss' x y ~= ss x y || ss' x y > ss x y
  -}
update vs x0 q0 x1 q1 =
  let
    s = vsLookup vs
    w = distributionProportions vs x0 x1
    r i j
      | i == x0   = 1 + q0 * w j
      | i == x1   = 1 + q1 * (s x0 x1 / s x1 x0) * (w x1 - w j)
      | otherwise =
        let
          r011i = r x0 x1 * r x1 i
          r100i = r x1 x0 * r x0 i
        in
        (r011i * w j + r100i * (w x1 - w j)) /
          (r011i * w i + r100i * (w x1 - w i))
    s' i j = s i j * r i j
  in
  fromFunction s' $ nodes vs

multiUpdate :: (Ord a, Ord b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> (a -> b) -> ValueSimplex a b
{- multiUpdate vs f should have the same set of nodes as vs and should have
    nodeValue (multiUpdate vs f) x ~= f x
  for all x in nodes vs.
  It should also affect linkValueSquared uniformly, as update does.  Ideally, it
  should spread this surplus (or deficit) as fairly as possible, like update,
  but this is hard to specify precisely, and may be in tension with
  computational complexity.
  -}
multiUpdate = fst .! multiUpdate'

multiUpdate' :: (Ord a, Ord b, Fractional b, MatrixElement b)
  => ValueSimplex a b -> (a -> b) -> (ValueSimplex a b, Bool)
multiUpdate' vs nv =
  let
    xs = nodes vs
    q x = nv x - nodeValue vs x
    xps = Set.filter ((>) <$> nv <*> nodeValue vs) xs
    xns = Set.filter ((<) <$> nv <*> nodeValue vs) xs
    mostValuableIncrease vs' x y =
      compare (q x * price vs' x y) $ q y
    evenSpread xs' vs' = flip fromFunction xs $ \x y ->
      if Set.member x xs'
      then vsLookup vs' x y * (nv x / nodeValue vs' x)
      else vsLookup vs' x y
    breakEvenUpdate vs' x0 q0 x1 = update vs' x0 q0 x1 $ breakEven vs' x0 q0 x1
    pileUp xmax' xps' vs' = case Set.minView xps' of
      Nothing -> vs'
      Just (x, xps'') -> pileUp xmax' xps'' $ breakEvenUpdate vs' x (q x) xmax'
    unPile xmax' xns' vs' =
      let
        xmin = maximumBy (mostValuableIncrease vs') xns'
          -- i.e., smallest decrease
        xns'' = Set.delete xmin xns'
        qxmax = nv xmax' - nodeValue vs' xmax'
        qxmin = q xmin
        beq = breakEven vs' xmax' qxmax xmin
      in
      if qxmin <= - supremumSellable vs' xmin xmax'
        || (not (Set.null xns'') && qxmin < beq)
      then (evenSpread xns' $ update vs' xmax' qxmax xmin beq, False)
      else if Set.null xns''
      then (update vs' xmax' qxmax xmin qxmin, qxmin > beq)
      else unPile xmax' xns'' $ breakEvenUpdate vs' xmin qxmin xmax'
  in
  if Set.null xps
  then (evenSpread xns vs, False)
  else if Set.null xns
  then (evenSpread xps vs, True)
  else
  let
    xmax = maximumBy (mostValuableIncrease vs) xps
  in
  unPile xmax xns $ pileUp xmax (Set.delete xmax xps) vs

linkOptimumAtPrice :: (Ord a, Fractional b)
  => ValueSimplex a b -> a -> a -> b -> (b, b)
{- linkOptimumAtPrice vs x0 x1 p == (q0, q1) should imply that q0 x0 should be
  bought in exchange for -q1 x1, with q1 = -p * q0, *assuming that the
  x0--x1 link is severed from the rest of vs*.  A negative q0 indicates that
  some x0 should be sold in exchange for x1 at that price.
  -}
linkOptimumAtPrice vs x0 x1 p =
  let
    s = vsLookup vs
    q1 = (/ 2) $ s x0 x1 * p - s x1 x0
  in
  (-q1 / p, q1)

linkBreakEvenAtPriceWithFee :: (Ord a, Ord b, Floating b)
  => ValueSimplex a b -> a -> a -> b -> b -> Maybe ((b, b), (b, b))
{- If
    status (~=) vs == OK,
    Set.member x0 $ nodes vs,
    Set.member x1 $ nodes vs,
    p > 0,
    s0 == vsLookup vs x0 x1,
    s1 == vsLookup vs x1 x0, and
    s0s1 == s0 * s1,
  then the following should hold:
    (a) in the case where
        linkBreakEvenAtPriceWithFee vs x0 x1 p f == Just ((q0, q1), (q0', q1')),
      we have:
        (s0 + q0 ) * (s1 + q1  - f) ~= s0s1,
        (s0 + q0') * (s1 + q1' - f) ~= s0s1,
        q1  ~= -p * q0,
        q1' ~= -p * q0',
        q1 <= q1',
        for all q strictly between q1 and q1',
          (s0 - q / p) * (s1 + q - f) > s0s1,
        for all q < q1,  (s0 - q / p) * (s1 + q - f) < s0s1, and
        for all q > q1', (s0 - q / p) * (s1 + q - f) < s0s1; and
    (b) in the case where
        linkBreakEvenAtPriceWithFee vs x0 x1 p f == Nothing,
      we have:
        for all q, (s0 - q / p) * (s1 + q - f) < s0s1.
  -}
linkBreakEvenAtPriceWithFee vs x0 x1 p f =
  let s0 = vsLookup vs x0 x1
      s1 = vsLookup vs x1 x0
      b = s0 * p - s1 + f
      disc = b * b - 4 * p * s0 * f
  in if disc < 0
    then Nothing
    else
      let q1  = (b - sqrt disc) / 2
          q1' = (b + sqrt disc) / 2
      in Just ((-q1 / p, q1), (-q1' / p, q1'))

totalValue :: (Ord a, Eq b, Fractional b) => ValueSimplex a b -> a -> b
-- The value of the ValueSimplex in terms of the given node
totalValue vs x = sumWith ((/) <$> nodeValue vs <*> price vs x) $ nodes vs

addNode :: (Ord a, Eq b, Fractional b)
  => ValueSimplex a b -> a -> b -> a -> b -> ValueSimplex a b
{-  vs' == addNode vs x q y p
  should imply:
    status (~=) vs' == OK
    nodes vs' == Set.insert x $ nodes vs
    nodeValue vs' x ~= q
    price vs' x y ~= p
  and for i, j, k, and l in nodes vs:
    nodeValue vs' i ~= nodeValue vs i
    price vs' i j ~= price vs i j
    ss' i j / ss' k l ~= ss i j / ss k l
      where
        ss = linkValueSquared vs
        ss' = linkValueSquared vs'
  assuming:
    status (~=) vs == OK
    Set.notMember x $ nodes vs
    Set.member y $ nodes vs
    p > 0
    q > 0
    q * p < totalValue vs y
  -}
addNode vs x q y p =
  let tv = totalValue vs y in
  flip fromFunction (Set.insert x $ nodes vs) $ \i j ->
  if i == x
  then q * nodeValue vs j * price vs j y / tv
  else if j == x
  then (q * p / tv) * nodeValue vs i
  else (1 - q * p / tv) * vsLookup vs i j

strictlySuperior :: (Ord a, Ord b, Num b)
  => (b -> b -> Bool) -> ValueSimplex a b -> ValueSimplex a b -> Bool
strictlySuperior eq vs vs' =
  let
    comparisons = flip Set.map (distinctPairs $ nodes vs) $ \(x, y) -> 
      let
        ssxy  = linkValueSquared vs   x y
        ss'xy = linkValueSquared vs'  x y
      in
      if ssxy `eq` ss'xy then EQ else compare ssxy ss'xy
  in
  Set.member GT comparisons && Set.notMember LT comparisons

deposit :: Ord a
  => ValueSimplex a Double -> (a -> Double) -> ValueSimplex a Double
{-  vs' == deposit vs f
  should imply:
    status (~=) vs' == OK,
    nodes vs' == nodes vs,
    nodeValue vs' x ~= f x, and
    (v' x y - v x y) * sqrt (price vs y x)
      ~= (v' z w - v z w) * sqrt (price vs z x * price vs w x)
  assuming:
    status (~=) vs == OK,
    f x >= nodeValue vs x,
    v = sqrt .! linkValueSquared vs, and
    v' = sqrt .! linkValueSquared vs'
  for all:
    distinct x, y in nodes vs, and
    distinct z, w in nodes vs
  -}
deposit vs@(VS vsm) f =
  let
    x0 = Set.findMin $ nodes vs
    tryIncrease v = multiUpdate'
      (VS $ Map.mapWithKey
        (\(x, _) -> ((monotonicWordToDouble v * price vs x0 x) +)) vsm)
      f
  in
  fst $ tryIncrease $ fromJust $ search $ not . snd . tryIncrease

